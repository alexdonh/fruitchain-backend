'use strict';

module.exports.watch = {
    active: true,
    usePolling: false,
    overrideMigrateSetting: false,
    dirs: [
      "api/controllers",
      "api/helpers",
      "api/hooks",
      "api/jobs",
      "api/models",
      "api/policies",
      "api/responses",
      "config",
    ],
    ignored: [
      // Ignore all files with .ts extension
      "**.ts"
    ],
    depth: 4
  };