'use strict';

/**
 * Error codes
 * 
 * Name and message can be changed from time to time
 * But code should NEVER once created because they may
 * be used in client for i18n / l10n purposes
 * 
 */

module.exports.errors = {
  // http error code mapping
  // only used for parsing / handling errors
  // codes that not in here will be treated as 500
  __MAPPING__: {
    40001: 400,
    40002: 500,
    40003: 401,
    40004: 401
  },

  // basic http codes
  BAD_REQUEST:                   { code: 400, message: 'Bad request' },
  UNAUTHORIZED:                  { code: 401, message: 'Unauthorized' },
  PAYMENT_REQUIRED:              { code: 402, message: 'Payment required' },
  FORBIDDEN:                     { code: 403, message: 'Forbidden' },
  NOT_FOUND:                     { code: 404, message: 'Not found' },
  NOT_ALLOWED:                   { code: 405, message: 'Method not allowed' },
  NOT_ACCEPTABLE:                { code: 406, message: 'Not acceptable' },
  PROXY_AUTHENTICATION_REQUIRED: { code: 407, message: 'Proxy authentication required' },
  REQUEST_TIMEOUT:               { code: 408, message: 'Request timeout' },
  CONFLICT:                      { code: 409, message: 'Conflict' },
  GONE:                          { code: 410, message: 'Gone' },
  TOO_MANY_REQUESTS:             { code: 429, message: 'Too many requests' },
  INTERNAL_SERVER_ERROR:         { code: 500, message: 'Internal server error' },
  NOT_IMPLEMENTED:               { code: 501, message: 'Not implemented' },
  BAD_GATEWAY:                   { code: 502, message: 'Bad gateway' },
  SERVICE_UNAVAILABLE:           { code: 503, message: 'Service unavailable' },
  GATEWAY_TIMEOUT:               { code: 504, message: 'Gateway timeout' },
  PROTOCOL_NOT_SUPPORTED:        { code: 505, message: 'HTTP protocol version not supported' },

  // form validation 3
  // PROP_IS_REQUIRED:          { code: 30001, message: '{property} cannot be left blank' },
  // PROP_TYPE_NUMBER:          { code: 30002, message: '{property} must be a number' },
  // PROP_TYPE_INTEGER:         { code: 30003, message: '{property} must be an integer' },
  // PROP_TYPE_STRING:          { code: 30004, message: '{property} must be a string' },
  // PROP_TYPE_BOOLEAN:         { code: 30005, message: '{property} must be either "{true}" or "{false}"' },
  // PROP_TYPE_ARRAY:           { code: 30006, message: '{property} must be an array' },
  // PROP_TYPE_OBJECT:          { code: 30007, message: '{property} must be an object' },
  // PROP_NUMBER_MAX:           { code: 30008, message: '{property} must be no greater than {max}' },
  // PROP_NUMBER_MIN:           { code: 30009, message: '{property} must be no less than {min}' },
  // PROP_NUMBER_EXCLUSIVE_MAX: { code: 30010, message: '{property} must be no greater than or equal {max}' },
  // PROP_NUMBER_EXCLUSIVE_MIN: { code: 30011, message: '{property} must be no less than or equal {min}' },
  // PROP_STRING_MAX_LENGTH:    { code: 30012, message: '{property} should contain at most {max} characters' },
  // PROP_STRING_MIN_LENGTH:    { code: 30013, message: '{property} should contain at least {min} characters' },
  // PROP_STRING_REGEX:         { code: 30014, message: '{property} is invalid' },
  // PROP_STRING_FORMAT:        { code: 30015, message: '{property} must be in {format} format' },
  // PROP_ARRAY_MAX_ITEMS:      { code: 30016, message: '{property} must have no more than {max} items' },
  // PROP_ARRAY_MIN_ITEMS:      { code: 30017, message: '{property} must have no less than {min} items' },
  // PROP_ARRAY_UNIQUE_ITEMS:   { code: 30018, message: '{property} must have unique items' },
  // PROP_OBJ_MAX_PROPS:        { code: 30019, message: '{property} must have no more than {max} properties' },
  // PROP_OBJ_MIN_PROPS:        { code: 30020, message: '{property} must have no less than {min} properties' },
  // PROP_ENUM:                 { code: 30021, message: '{property} must be one of the following values: {values}' },
  // PROP_CONST:                { code: 30022, message: '{property} must be exactly {value}' },

  
  // authentication 4
  LOGIN_UNSUCCESSFUL: { code: 40001, message: 'Invalid username and/or password' },
  TOKEN_UNAVAILABLE:  { code: 40002, message: 'Cannot issue access token' },
  TOKEN_INVALID:      { code: 40003, message: 'Access token is invalid' },
  TOKEN_EXPIRED:      { code: 40004, message: 'Access token has already expired' },

  // supply chain 5

  // others 9
}