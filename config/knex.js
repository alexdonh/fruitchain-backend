'use strict';

/**
 * Knex configuration
 * (sails.config.knex)
 *
 * Knex settings
 *
 * For more information on knex configuration, visit:
 * https://knexjs.org/
 */

module.exports.knex = {
  client: 'pg',
  connection: 'postgresql://postgres:c9BqhGZM5v7EPTs7@localhost:5432/sails',
  migrations: {
    extension: 'js',
    tableName: 'migration',
  }
}