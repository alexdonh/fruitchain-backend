'use strict';

/**
 * Configuration for sawtooth
 * (sails.config.sawtooth)
 */

module.exports.sawtooth = {

  familyName: 'fruitchain',
  familyVersion: '2.0',
  privateKey: 'af22c3fb25c24830516e553f789e7ddd7fef3f0fe68a40f723c326c2ed5fb5a1',

};
