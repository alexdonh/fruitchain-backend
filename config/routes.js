'use strict';

/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  'POST /v2/auth/login':   'v2/AuthController.login',
  'GET /v2/auth/logout':   'v2/AuthController.logout',
  'POST /v2/auth/refresh': 'v2/AuthController.refresh',
  'GET /v2':               'v2/DefaultController.index',
};
