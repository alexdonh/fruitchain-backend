openapi: 3.0.2
info:
  title: Fruichain
  description: Well-documented API specification for Fruitchain
  version: 2.0.0
servers:
  - url: https://localhost:3000/v2
    description: Development
components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  parameters:
    locale:
      in: header
      name: X-Locale
      required: true
      schema:
        type: string
        enum: [en, vi, ja, zh]
  schemas:
    Error:
      type: object
      properties:
        code:
          type: integer
        message:
          type: string
    List:
      type: object
      properties:
        total: 
          type: integer
        pages:
          type: integer
        page:
          type: integer
        perPage:
          type: integer
        items:
          type: array
          items:
            type: string
    Organization:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        name:
          type: string
        description:
          type: string
        address1:
          type: string
        address2:
          type: string
        city:
          type: string
        state:
          type: string
        country:
          type: string
        email:
          type: string
        phone:
          type: string
        fax:
          type: string
        taxNo:
          type: string
        registrationNo:
          type: string
    User:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        username:
          type: string
        status:
          type: integer
        contact:
          $ref: '#/components/schemas/Contact'
    Contact:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        organizationId:
          type: integer
        organization:
          $ref: '#/components/schemas/Organization'
        gender:
          type: string
          enum: [M, F, O]
        initial:
          type: string
          enum: [Mr., Mrs., Ms]
        firstName:
          type: string
        lastName:
          type: string
        nationality:
          type: string
        email:
          type: string
        phone:
          type: string
    Batch:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        uuid:
          type: string
        productTypeId:
          type: integer
        productType:
          $ref: '#/components/schemas/ProductType'
        productCategoryId:
          type: integer
        productCategory:
          $ref: '#/components/schemas/ProductCategory'
        userId:
          type: integer
        user:
          $ref: '#/components/schemas/User'
        number:
          type: string
        description:
          type: string
        productionDate:
          type: string
          format: date
        status:
          type: integer
    ProductType:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        uuid:
          type: string
        name:
          type: string
        description:
          type: string
        productCategories:
          type: array
          items:
            $ref: '#/components/schemas/ProductCategory'
    ProductCategory:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        uuid:
          type: string
        name:
          type: string
        description:
          type: string
        parentId:
          type: integer
        parent:
          $ref: '#/components/schemas/ProductCategory'
          nullable: true
        productTypeId:
          type: integer
        productType:
          $ref: '#/components/schemas/ProductType'
    Sticker:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        code:
          type: string
        type:
          type: integer
          enum: [1,2]
        stickerOrderId:
          type: integer
        stickerOrder:
          $ref: '#/components/schemas/StickerOrder'
        stickerOrderItemId:
          type: integer
        stickerOrderItem:
          $ref: '#/components/schemas/StickerOrderItem'
        parentId:
          type: integer
        parent:
          $ref: '#/components/schemas/Sticker'
          nullable: true
        userId:
          type: integer
        user:
          $ref: '#/components/schemas/User'
        ownerId:
          type: integer
        owner:
          $ref: '#/components/schemas/User'
        status:
          type: integer
    StickerPrice:
      type: object
      properties:
        type:
          type: integer
        price:
          type: number
    StickerOrder:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        number:
          type: string
        userId:
          type: integer
        user:
          $ref: '#/components/schemas/User'
        downloadUrl:
          type: string
        qty:
          type: integer
        total:
          type: decimal
        status:
          type: integer
        stickerOrderItems:
          type: array
          items:
            $ref: '#/components/schemas/StickerOrderItem'
    StickerOrderItem:
      type: object
      properties:
        id:
          type: integer
        stickerType:
          type: integer
          enum: [1,2]
        stickerOrderId:
          type: integer
        stickerOrder:
          $ref: '#/components/schemas/StickerOrder'
        productTypeId:
          type: integer
        productType:
          $ref: '#/components/schemas/ProductType'
        qty:
          type: integer
        unit_price:
          type: decimal
        total:
          type: decimal
        increments:
          type: integer
    DeliveryOrder:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        number:
          type: string
        transferorId:
          type: integer
        transferor:
          $ref: '#/components/schemas/User'
        transfereeId:
          type: integer
        transferee:
          $ref: '#/components/schemas/User'
        courierId:
          type: integer
        courier:
          $ref: '#/components/schemas/Courier'
        status:
          type: integer
        deliveryOrderItems:
          type: array
          items:
            $ref: '#/components/schemas/DeliveryOrderItem'
    DeliveryOrderItem:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        deliveryOrderId:
          type: integer
        deliveryOrder:
          $ref: '#/components/schemas/DeliveryOrder'
        productTypeId:
          type: integer
        productType:
          $ref: '#/components/schemas/ProductType'
        productCategoryId:
          type: integer
        productCategory:
          $ref: '#/components/schemas/ProductCategory'
        qty:
          type: integer
        status:
          type: integer
        stickers:
          type: array
          items:
            $ref: '#/components/schemas/Sticker'
    Courier:
      type: object
      properties:
        createdAt:
          type: string
          format: date-time
        updatedAt:
          type: string
          format: date-time
        id:
          type: integer
        name:
          type: string
        address1:
          type: string
        address2:
          type: string
        city:
          type: string
        state:
          type: string
        country:
          type: string
        email:
          type: string
        phone:
          type: string
        userId:
          type: integer
        user:
          $ref: '#/components/schemas/User'
          nullable: true
security:
  - bearerAuth: []
paths:
  /auth/login:
    post:
      summary: Login
      tags: 
        - Authentication
      parameters:
        - name: username
          description: Username
          in: query
          required: true
          schema:
            type: string
        - name: password
          description: Password
          in: query
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema: 
                type: object
                properties:
                  token: 
                    type: string
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /auth/logout:
    get:
      summary: Logout
      tags: 
        - Authentication
      responses:
        '200':
          description: Logout successfully
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /auth/refresh:
    post:
      summary: Get a new JWT
      tags: 
        - Authentication
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema: 
                type: object
                properties:
                  token: 
                    type: string
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /products:
    get:
      summary: Get product list
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/ProductType'
  /products/{id}:
    get:
      summary: Get a specific product
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductType'
  /products/{id}/categories:
    get:
      summary: Get category list by specific product
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/Category'
  /categories:
    get:
      summary: Get category list
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/Category'
  /batches:
    get:
      summary: Get today batches
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/Batch'
    post:
      summary: Add a new batch
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
        - in: query
          name: name
          required: true
          schema:
            type: string
        - in: query
          name: description
          required: true
          schema:
            type: string
        - in: query
          name: productId
          required: true
          schema:
            type: integer
        - in: query
          name: categoryId
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Batch'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /batches/{id}:
    get: 
      summary: Get a specific batch
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Batch'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
    put:
      summary: Update a batch
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
        - in: path
          name: id
          required: true
          schema:
            type: integer
        - in: query
          name: name
          required: true
          schema:
            type: string
        - in: query
          name: description
          required: true
          schema:
            type: string
        - in: query
          name: productId
          required: true
          schema:
            type: integer
        - in: query
          name: categoryId
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Batch'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /sticker-orders:
    get:
      summary: Get sticker order list
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/StickerOrder'
    post:
      summary: Add a new sticker order
      tags:
        - Supply Chain
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/StickerOrder'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /sticker-orders/{id}:
    get:
      summary: Get a specific sticker order
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/StickerOrder'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
    put:
      summary: Update a sticker order
      tags:
        - Supply Chain
      parameters: {}
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/StickerOrder'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
    delete:
      summary: Delete a sticker order
      tags:
        - Supply Chain
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /sticker-orders/{id}/reject:
    post:
      summary: Reject a sticker order
      tags:
        - Supply Chain
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
        - in: query
          name: reason
          schema:
            type: string
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /sticker-orders/{id}/accept:
    post:
      summary: Accept a sticker order
      tags:
        - Supply Chain
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /sticker-orders/{id}/confirm:
    post:
      summary: Confirm to receive stickers generated base on this order
      tags:
        - Supply Chain
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /sticker-orders/{id}/generate:
    post:
      summary: Queue a job to generate stickers base on this order
      tags:
        - Supply Chain
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
  /stickers:
    get:
      summary: Get sticker list
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/Sticker'
  /stickers/prices:
    get:
      summary: Get sticker prices base on types
      tags:
        - Supply Chain
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/StickerPrice'
  /stickers/activate:
    post:
      summary: Activate stickers
      tags:
        - Supply Chain
      parameters: []
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /stickers/validate:
    post:
      summary: Validate stickers when scan to activate
      tags:
        - Supply Chain
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
        - in: query
          name: stickers
          required: true
          schema:
            type: array
            items:
              type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  valid:
                    type: array
                    items: 
                      type: string
                  invalid:
                    type: array
                    items:
                      allOf:
                        - $ref: '#/components/schemas/Error'
                        - type: object
                          properties:
                            stickers:
                              type: array
                              items:
                                type: string
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /delivery-orders:
    get:
      summary: Get delivery order list
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/DeliveryOrder'
    post:
      summary: Add a new delivery order
      tags:
        - Supply Chain
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeliveryOrder'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /delivery-orders/{id}:
    get:
      summary: Get a specific delivery order
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeliveryOrder'
        'xxx':
            description: Error
            content:
              application/json:
                schema: 
                  type: array
                  items:
                    $ref: '#/components/schemas/Error'
    put:
      summary: Update a delivery order
      tags:
        - Supply Chain
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeliveryOrder'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
    delete:
      summary: Delete a delivery order
      tags:
        - Supply Chain
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /delivery-orders/{id}/stickers:
    get:
      summary: Get stickers belonging to this delivery order
      tags:
        - Supply Chain
      parameters:
        - $ref: '#/components/parameters/locale'
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/Sticker'
  /delivery-orders/{id}/stickers/validate:
    get:
      summary: Validate stickers belonging to this delivery order when scan on delivery
      tags:
        - Supply Chain
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
        - in: query
          name: stickers
          required: true
          schema:
            type: array
            items:
              type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  valid:
                    type: array
                    items: 
                      type: string
                  invalid:
                    type: array
                    items:
                      allOf:
                        - $ref: '#/components/schemas/Error'
                        - type: object
                          properties:
                            stickers:
                              type: array
                              items:
                                type: string
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /delivery-orders/{id}/confirm:
    post:
      summary: Confirm to receive this delivery order
      tags:
        - Supply Chain
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /users:
    get:
      summary: Get user list
      tags:
        - KYC
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/User'
  /users/register:
    post:
      summary: Register new user account
      tags:
        - KYC
      parameters: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /users/{id}:
    get:
      summary: Get a specific user
      tags:
        - KYC
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /contacts/has-roles:
    get:
      summary: Get contacts has specified roles
      tags:
        - KYC
        - Supply Chain
      parameters:
        - in: query
          name: roles
          required: true
          schema:
            type: array
            items:
              type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/User'                
  /contacts/{id}:
    get:
      summary: Get a specific contact
      tags:
        - KYC
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Contact'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /organizations:
    get:
      summary: Get organization list
      tags:
        - KYC
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/List'
                  - type: object
                    properties:
                      items:
                        type: array
                        items:
                          $ref: '#/components/schemas/Organization'
  /organizations/{id}:
    get:
      summary: Get a specific organization
      tags:
        - KYC
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Organization'
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /users/{id}/register-key:
    put:
      summary: Register public key
      tags:
        - KYC
      parameters: 
        - in: query
          name: publicKey
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /traceability/stickers/{id}:
    get:
      summary: Get sticker traceable information
      tags:
        - Explorer
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /traceability/organizations/{id}:
    get:
      summary: Get organization traceable information
      tags:
        - Explorer
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /stickers/analytics:
    put:
      summary: Collect end-user information for analytics
      tags:
        - Analytics
      parameters:
        - in: query
          name: code
          required: true
          schema:
            type: string
        - in: query
          name: ip
          required: true
          schema:
            type: string
        - in: query
          name: latitude
          schema:
            type: number
        - in: query
          name: longitude
          schema:
            type: number
        - in: query
          name: city
          schema:
            type: string
        - in: query
          name: country
          schema:
            type: string
        - in: query
          name: device
          schema:
            type: string
        - in: query
          name: os
          schema:
            type: string
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'
  /users/analytics:
    put:
      summary: Collect end-user information for analytics
      tags:
        - Analytics
      parameters:
        - in: query
          name: id
          required: true
          schema:
            type: integer
        - in: query
          name: code
          required: true
          schema:
            type: string
        - in: query
          name: ip
          required: true
          schema:
            type: string
        - in: query
          name: latitude
          schema:
            type: number
        - in: query
          name: longitude
          schema:
            type: number
        - in: query
          name: city
          schema:
            type: string
        - in: query
          name: country
          schema:
            type: string
        - in: query
          name: device
          schema:
            type: string
        - in: query
          name: os
          schema:
            type: string
      responses:
        '200':
          description: OK
        'xxx':
          description: Error
          content:
            application/json:
              schema: 
                type: array
                items:
                  $ref: '#/components/schemas/Error'