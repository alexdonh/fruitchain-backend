
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('plan', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.increments();
      table.string('name', 255);
      table.text('description');
      table.specificType('status', 'smallint').notNullable().defaultTo(1);
      // other plan settings will be added later
      // e.g. max users, max product category...
    })
    .createTable('plan_translation', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.specificType('locale', 'char(2)').notNullable();
      table.integer('plan_id', 255).notNullable();
      table.string('name', 255);
      table.text('description');
      table.primary(['locale', 'plan_id']);
      table.foreign('plan_id').references('id').inTable('plan').onDelete('CASCADE').onUpdate('CASCADE');
    })
    .createTable('user_plan', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.increments();
      table.bigInteger('user_id').notNullable();
      table.integer('plan_id').notNullable();
      table.timestamp('valid_at').notNullable();
      table.timestamp('expired_at');
      table.specificType('status', 'smallint').notNullable().defaultTo(0);
      table.foreign('user_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('plan_id').references('id').inTable('plan').onDelete('RESTRICT').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('user_plan')
    .dropTable('plan_translation')
    .dropTable('plan');
};
