
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('organization_certificate', function(table) {
    table.integer('product_type_id').notNullable();
    table.integer('product_category_id');
    table.foreign('product_type_id').references('id').inTable('product_type').onDelete('RESTRICT').onUpdate('CASCADE');
    table.foreign('product_category_id').references('id').inTable('product_category').onDelete('RESTRICT').onUpdate('CASCADE');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('organization_certificate', function(table) {
    table.dropForeign('product_type_id');
    table.dropForeign('product_category_id');
    table.dropColumn('product_type_id');
    table.dropColumn('product_category_id');
  });
};