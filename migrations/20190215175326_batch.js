
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('batch', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigIncrements();
      table.uuid('uuid').unique().notNullable();
      table.integer('product_type_id').notNullable();
      table.integer('product_category_id').notNullable();
      table.bigInteger('user_id').notNullable();
      table.string('number', 36).notNullable();
      table.text('description');
      table.datetime('production_date').notNullable();
      table.specificType('status', 'smallint').notNullable().defaultTo(0);
      table.foreign('product_type_id').references('id').inTable('product_type').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('product_category_id').references('id').inTable('product_category').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('user_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
    })
    .alterTable('sticker', function(table) {
      table.bigInteger('batch_id');
      table.foreign('batch_id').references('id').inTable('batch').onDelete('RESTRICT').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .alterTable('sticker', function(table) {
      table.dropForeign('batch_id');
      table.dropColumn('batch_id');
    })
    .dropTable('batch');
};
