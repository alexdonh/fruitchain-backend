'use strict';


exports.up = function(knex, Promise) {
  return knex.schema.createTable('access_token', function(table) {
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.bigIncrements();
    table.bigInteger('user_id');
    table.text('token').notNullable(); // should check uniqueness by code because some db doesnt support unique on text/blob
    table.string('ip', 45).notNullable();
    table.string('agent', 255);
    table.boolean('revoked').defaultTo(false);
    table.boolean('expired').defaultTo(false);
    table.foreign('user_id').references('id').inTable('user').onDelete('CASCADE').onUpdate('CASCADE');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('access_token');
};
