
exports.up = function(knex, Promise) {
  return knex.schema.createTable('organization', function(table) {
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.bigIncrements();
    table.string('name', 255).unique().notNullable();
    table.text('description');
    table.string('address1', 255);
    table.string('address2', 255);
    table.string('city', 255);
    table.string('state', 255);
    table.string('country', 255);
    table.string('email', 100);
    table.string('phone', 20);
    table.string('fax', 20);
    table.string('tax_no', 50);
    table.string('registration_no', 50);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('organization');
};
