
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('organization', function(table) {
    table.text('logo');
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('organization', function(table) {
    table.dropColumn('logo');
  })
};