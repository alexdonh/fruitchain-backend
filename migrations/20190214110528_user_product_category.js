
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('user_product_category', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigInteger('user_id');
      table.integer('product_category_id');
      table.primary(['product_category_id', 'user_id']);
      table.foreign('user_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('product_category_id').references('id').inTable('product_category').onDelete('RESTRICT').onUpdate('CASCADE');
    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('user_product_category');
};
