
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('channel', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.increments();
      table.integer('plan_id').notNullable();
      table.string('name', 255);
      table.string('endpoint', 40);
      table.json('nodes');
      table.json('specs');
      table.specificType('status', 'smallint').notNullable().defaultTo(1);
      table.foreign('plan_id').references('id').inTable('plan').onDelete('RESTRICT').onUpdate('CASCADE');
      // other plan settings will be added later
      // e.g. max users, max product category...
    })
    .createTable('user_channel', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigInteger('user_id').notNullable();
      table.integer('channel_id').notNullable();
      table.integer('product_type_id').notNullable();
      table.integer('product_category_id').notNullable();
      table.primary(['user_id', 'channel_id', 'product_type_id', 'product_category_id']);
      table.foreign('user_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('channel_id').references('id').inTable('channel').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('product_type_id').references('id').inTable('product_type').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('product_category_id').references('id').inTable('product_category').onDelete('RESTRICT').onUpdate('CASCADE');
    })
    .alterTable('sticker', function(table) {
      table.integer('channel_id');
      table.foreign('channel_id').references('id').inTable('channel').onDelete('RESTRICT').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .alterTable('sticker', function(table) {
      table.dropForeign('channel_id');
      table.dropColumn('channel_id');
    })
    .dropTable('user_channel')
    .dropTable('channel');
};
