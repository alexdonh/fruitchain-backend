'use strict';


exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('auth', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.string('id', 255).collate('latin1_general_ci').unique().primary();
      table.specificType('type', 'smallint').defaultTo(2);
      table.string('name', 255);
      table.text('description');
      table.json('data');
    })
    .createTable('auth_child', function(table) {
      table.string('parent_id', 255).collate('latin1_general_ci');
      table.string('child_id', 255).collate('latin1_general_ci');
      table.primary(['parent_id', 'child_id']);
      table.foreign('parent_id').references('id').inTable('auth').onDelete('CASCADE').onUpdate('CASCADE');
      table.foreign('child_id').references('id').inTable('auth').onDelete('CASCADE').onUpdate('CASCADE');
    })
    .createTable('user_auth', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigInteger('user_id');
      table.string('auth_id', 255).collate('latin1_general_ci');
      table.primary(['user_id', 'auth_id']);
      table.foreign('user_id').references('id').inTable('user').onDelete('CASCADE').onUpdate('CASCADE');
      table.foreign('auth_id').references('id').inTable('auth').onDelete('CASCADE').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('user_auth')
    .dropTable('auth_child')
    .dropTable('auth')
};
