
exports.up = function(knex, Promise) {
  return knex.schema.createTable('auth_translation', function(table) {
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.specificType('locale', 'char(2)').notNullable();
    table.string('auth_id', 255).collate('latin1_general_ci').notNullable();
    table.string('name', 255);
    table.text('description');
    table.primary(['locale', 'auth_id']);
    table.foreign('auth_id').references('id').inTable('auth').onDelete('CASCADE').onUpdate('CASCADE');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('auth_translation');
};
