
exports.up = function(knex, Promise) {
  return knex.schema.createTable('contact', function(table) {
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.bigInteger('id').primary();
    table.bigInteger('organization_id').notNullable();
    table.specificType('gender', 'char(1)');
    table.string('initial', 5);
    table.string('first_name', 50);
    table.string('last_name', 50);
    table.string('nationality', 50);
    table.string('email', 100);
    table.string('phone', 20);
    table.foreign('organization_id').references('id').inTable('organization').onDelete('RESTRICT').onUpdate('CASCADE');
    table.foreign('id').references('id').inTable('user').onDelete('CASCADE').onUpdate('CASCADE');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('contact');
};
