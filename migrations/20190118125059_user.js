'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.createTable('user', function(table) {
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.bigIncrements();
    table.string('username', 64).unique().notNullable();
    table.string('password', 255).notNullable();
    table.string('reset_token', 255);
    table.specificType('status', 'smallint').defaultTo(0);
    table.string('public_key', 255);
    table.specificType('locale', 'char(2)').defaultTo('en');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('user');
};
