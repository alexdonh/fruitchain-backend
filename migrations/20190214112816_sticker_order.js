
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('sticker_order', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigIncrements();
      table.string('number', 36);
      table.bigInteger('user_id').notNullable();
      table.string('download_url', 255);
      table.integer('qty');
      table.decimal('total', 20, 4);
      table.specificType('status', 'smallint').notNullable().defaultTo(0);
      table.foreign('user_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
    })
    .createTable('sticker_order_item', function(table) {
      table.bigIncrements();
      table.specificType('sticker_type', 'smallint').notNullable().defaultTo(1);
      table.bigInteger('sticker_order_id');
      table.integer('product_type_id');
      //table.integer('product_category_id');
      table.integer('qty');
      table.decimal('unit_price', 20, 4);
      table.decimal('total', 20, 4);
      table.bigInteger('increments').notNullable().defaultTo(0);
      table.unique(['sticker_type', 'sticker_order_id', 'product_type_id']);
      table.foreign('sticker_order_id').references('id').inTable('sticker_order').onDelete('CASCADE').onUpdate('CASCADE');
      table.foreign('product_type_id').references('id').inTable('product_type').onDelete('RESTRICT').onUpdate('CASCADE');
      //table.foreign('product_category_id').references('id').inTable('product_category').onDelete('RESTRICT').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('sticker_order_item')
    .dropTable('sticker_order');
};
