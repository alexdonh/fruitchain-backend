
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('certificate', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.increments();
      table.string('name', 255);
      table.text('logo');
      table.text('description');
    })
    .createTable('organization_certificate', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigIncrements();
      table.integer('certificate_id').notNullable();
      table.bigInteger('organization_id').notNullable();
      table.bigInteger('issuer_id').notNullable();
      table.timestamp('valid_at').notNullable();
      table.timestamp('expired_at');
      table.foreign('organization_id').references('id').inTable('organization').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('certificate_id').references('id').inTable('certificate').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('issuer_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('organization_certificate')
    .dropTable('certificate');
};
