
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('courier', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.increments();
      table.string('name', 255).notNullable();
      table.string('address1', 255);
      table.string('address2', 255);
      table.string('city', 255);
      table.string('state', 255);
      table.string('country', 255);
      table.string('email', 100);
      table.string('phone', 20).notNullable();
      table.bigInteger('user_id');
      table.foreign('user_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
    })
    .createTable('delivery_order', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigIncrements();
      table.string('number', 36);
      table.bigInteger('transferor_id').notNullable();
      table.bigInteger('transferee_id').notNullable();
      table.integer('courier_id').notNullable();
      table.specificType('status', 'smallint').notNullable().defaultTo(0);
      table.foreign('transferor_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('transferee_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('courier_id').references('id').inTable('courier').onDelete('RESTRICT').onUpdate('CASCADE');
    })
    .createTable('delivery_order_item', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigIncrements();
      table.bigInteger('delivery_order_id');
      table.integer('product_type_id');
      table.integer('product_category_id');
      table.integer('qty');
      table.specificType('status', 'smallint').notNullable().defaultTo(0);
      table.unique(['delivery_order_id', 'product_type_id', 'product_category_id']);
      table.foreign('delivery_order_id').references('id').inTable('delivery_order').onDelete('CASCADE').onUpdate('CASCADE');
      table.foreign('product_type_id').references('id').inTable('product_type').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('product_category_id').references('id').inTable('product_category').onDelete('RESTRICT').onUpdate('CASCADE');
    })
    .createTable('delivery_order_item_sticker', function(table) {
      table.bigInteger('delivery_order_item_id');
      table.bigInteger('sticker_id');
      table.primary(['delivery_order_item_id', 'sticker_id']);
      table.foreign('delivery_order_item_id').references('id').inTable('delivery_order_item').onDelete('CASCADE').onUpdate('CASCADE');
      table.foreign('sticker_id').references('id').inTable('sticker').onDelete('RESTRICT').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('delivery_order_item_sticker')
    .dropTable('delivery_order_item')
    .dropTable('delivery_order')
    .dropTable('courier');
};
