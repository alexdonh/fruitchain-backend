
exports.up = function(knex, Promise) {
  return knex.schema.createTable('sticker_view', function(table) {
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.bigIncrements();
    table.bigInteger('sticker_id').notNullable();
    table.string('ip', 40);
    table.float('latitude', 8);
    table.float('longitude', 8);
    table.string('city');
    table.specificType('country', 'CHAR(2)');
    table.string('device', 255);
    table.string('os', 20);
    table.foreign('sticker_id').references('id').inTable('sticker').onDelete('CASCADE').onUpdate('CASCADE');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('sticker_view');
};
