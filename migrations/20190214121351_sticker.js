
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('sticker', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigIncrements();
      table.string('code', 64).unique().notNullable();
      table.specificType('type', 'smallint').notNullable().defaultTo(1);
      table.bigInteger('parent_id');
      table.bigInteger('user_id').notNullable();
      table.bigInteger('owner_id').notNullable();
      table.bigInteger('sticker_order_id').notNullable();
      table.bigInteger('sticker_order_item_id').notNullable();
      table.specificType('status', 'smallint').notNullable().defaultTo(0);
      table.foreign('parent_id').references('id').inTable('sticker').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('user_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('owner_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('sticker_order_id').references('id').inTable('sticker_order').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('sticker_order_item_id').references('id').inTable('sticker_order_item').onDelete('RESTRICT').onUpdate('CASCADE');
    })
    .createTable('sticker_status', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.bigIncrements();
      table.bigInteger('sticker_id').notNullable();
      table.bigInteger('transferor_id').notNullable();
      table.bigInteger('transferee_id').notNullable();
      table.specificType('old_status', 'smallint');
      table.specificType('new_status', 'smallint').notNullable();
      table.foreign('sticker_id').references('id').inTable('sticker').onDelete('CASCADE').onUpdate('CASCADE');
      table.foreign('transferor_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('transferee_id').references('id').inTable('user').onDelete('RESTRICT').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('sticker_status')
    .dropTable('sticker');
};
