
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('product_type', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.increments();
      table.uuid('uuid').unique().notNullable();
      table.string('name', 255).notNullable();
      table.text('description');
    })
    .createTable('product_category', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.increments();
      table.uuid('uuid').unique().notNullable();
      table.integer('product_type_id').notNullable();
      table.integer('parent_id');
      table.string('name', 255);
      table.text('description');
      table.foreign('product_type_id').references('id').inTable('product_type').onDelete('RESTRICT').onUpdate('CASCADE');
      table.foreign('parent_id').references('id').inTable('product_category').onDelete('SET NULL').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('product_category')
    .dropTable('product_type');
};
