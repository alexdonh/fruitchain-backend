
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('product_type_translation', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.specificType('locale', 'char(2)').notNullable();
      table.integer('product_type_id').notNullable();
      table.string('name', 255);
      table.text('description');
      table.primary(['locale', 'product_type_id']);
      table.foreign('product_type_id').references('id').inTable('product_type').onDelete('CASCADE').onUpdate('CASCADE');
    })
    .createTable('product_category_translation', function(table) {
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
      table.specificType('locale', 'char(2)').notNullable();
      table.integer('product_category_id').notNullable();
      table.string('name', 255);
      table.text('description');
      table.primary(['locale', 'product_category_id']);
      table.foreign('product_category_id').references('id').inTable('product_category').onDelete('CASCADE').onUpdate('CASCADE');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('product_category_translation')
    .dropTable('product_type_translation');
};
