'use strict';

module.exports = {
  friendlyName: 'Test',
  description: 'Uh oh I\'m just a test',
  fn: async function () {
    try {
      User.fromJson({});
    }
    catch(err) {
      sails.log.error(err.data);
    }
  }
};