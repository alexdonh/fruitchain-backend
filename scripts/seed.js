'use strict';

const path = require('path');
const moment = require('moment');

module.exports = {
  friendlyName: 'Seed',
  description: 'Initial seed',
  args: ['command', 'name'],
  inputs: {
    command: { type: 'string' },
    name: { type: 'string' },
  },
  fn: async function ({ command, name }) {
    const knex = require('knex')(sails.config.knex);
    sails.config.paths.seeds = path.resolve(sails.config.appPath, sails.config.paths.seeds);

    if (command === 'new') {
      if (!name) {
        sails.log.error('Seed name is required.');
        return;
      }

      name = moment().format('YYYYMMDDHHmmss') + '_' + name;

      return knex.seed
        .make(name, { directory: path.join(sails.config.paths.seeds, sails.config.environment) })
        .then((name) => {
          sails.log.info(`Created Seed: ${name}`);
        })
        .catch((err) => {
          sails.log.error(err);
        });
    }

    sails.log.info('Seeding...');
    return knex.seed
      .run({ directory: path.join(sails.config.paths.seeds, sails.config.environment) })
      .then(() => {
        sails.log.info('Done.');
      })
      .catch((err) => {
        sails.log.error(err);
      });
  }
};

