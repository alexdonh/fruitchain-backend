'use strict';

module.exports = {
  friendlyName: 'Migration',
  description: 'Manage database migrations based on knex.',
  args: ['command', 'name'],
  inputs: {
    command: { type: 'string', required: true },
    name: { type: 'string' }
  },
  fn: async function ({ command, name }) {

    const knex = require('knex')(sails.config.knex);

    switch(command) {

      default:
        sails.log.error('CLI not supported.');
        return;

      case 'new':
        return knex.migrate
          .make(name)
          .then((name) => {
            sails.log.info(`Created migration: ${name}`);
          })
          .catch((err) => {
            sails.log.error(err);
          });

      case 'up':
        return knex.migrate
          .latest()
          .spread((batch, log) => {
            if (log.length === 0) {
              sails.log.info('Already up to date');
              return;
            }
            sails.log.info(`Batch ${batch} run: ${log.length} migrations` + `\n${log.join('\n')}`)
          });

      case 'down':
        return knex.migrate
          .rollback()
          .spread((batch, log) => {
            if (log.length === 0) {
              sails.log.info('Already at the base migration');
              return;
            }
            sails.log.info(`Batch ${batch} rolled back: ${log.length} migrations` + `\n${log.join('\n')}`)
          });

      case 'current':
        return knex.migrate
          .currentVersion()
          .then((version) => {
            sails.log.info(`Current version: ${version}`);
          })
          .catch((err) => {
            sails.log.error(err);
          });
    }

  }
};

