'use strict';

const faker = require('faker');

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return User.query().delete()
    .then(function () {
      let users = [{ username: 'admin', password: '123456', status: 1 }];
      const rand = Math.floor(Math.random() * 1000) + 1;
      for(let i = 0; i < rand; ++i) {
        users.push({ username: faker.internet.userName(), password: '123456', status: 1 });
      }
      return User.query().insert(users);
    });
};
