'use strict';

/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const errors = sails.config.errors;

module.exports = {
  
  login: async function (req, res) {
    const { username, password } = req.body;
    const user = await User.query().findOne({ username });
    if (!user) {
      return res.error(errors.LOGIN_UNSUCCESSFUL);
    }
    if (!await sails.helpers.password.compare(password, user.password)) {
      // consider: limit failed attempts to, for example, 5 only
      // after those failed attempts, system could temporarily block all requests
      return res.error(errors.LOGIN_UNSUCCESSFUL);
    }

    try {
      const token = await sails.helpers.jwt.sign(user.toJSON());
      await user.$relatedQuery('accessTokens').insert({ token: token, ip: req.ip, agent: _.get(req.headers, 'user-agent') });
      res.cookie('token', token, { maxAge: 86400, httpOnly: true });
      return res.json({ token });
    } catch (err) {
      sails.log.error(err);
      return res.error(errors.TOKEN_UNAVAILABLE);
    }
  },

  logout: async function (req, res) {
    const parts = req.headers.authorization.split(' ');
    if (parts.length !== 2) {
      return res.error(errors.UNAUTHORIZED)
    }

    if(!/^Bearer$/i.test(parts[0])) {
      return res.error(errors.UNAUTHORIZED)
    }

    const token = parts[1];

    await AccessToken.query().where({ token }).update({ revoked: true });

    res.deleteCookie('token');
    return res.ok();
  },

  refresh: async function(req, res) {
    const user = await User.query().findOne({ id: req.user.id });
    if (!user) {
      return res.error(errors.TOKEN_INVALID);
    }
    try {
      const token = await sails.helpers.jwt.sign(user.toJSON());
      await user.$relatedQuery('accessTokens').insert({ token: token, ip: req.ip, agent: _.get(req.headers, 'user-agent') });
      res.cookie('token', token, { maxAge: 86400, httpOnly: true });
      await AccessToken.query().where({ token: req.user.token }).update({ expired: true });
      return res.json({ token });
    } catch (err) {
      sails.log.error(err);
      return res.error(errors.TOKEN_UNAVAILABLE);
    }
  }

};

