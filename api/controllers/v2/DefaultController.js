'use strict';

/**
 * DefaultController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
  index: async function (req, res) {
    //return res.ok({ foo: 'bar', hello: 'world' })
    return res.ok(await User.query().paginate(req.param('page', 1), 100));
  }

};