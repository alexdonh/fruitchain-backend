'use strict';

const User = require('../hooks/rbac/User');

module.exports = async function (req, res, next) {
  const errors = sails.config.errors;

  if (!req.user || !req.user instanceof User) {
    return res.error(errors.UNAUTHORIZED);
  }

  if (req.user.isGuest && req.path === req.user.loginUrl) {
    return next();
  }

  const method = req.method.toUpperCase();
  let action = req.options.action;

  sails.log.info(`Checking access to: ${method} /${action}`);

  if (await req.user.can(`${method} /${action}`) || 
      await req.user.can(`/${action}`)) {
    return next();
  }

  action = action.split('/');
  action.pop();

  do {
    let a = action.join('/');
    sails.log.info(`Checking access to: ${method} /${a}`);
    if (await req.user.can(`${method} /${a}`) || 
        await req.user.can(`/${a}`) ||
        await req.user.can(`${method} /${a}/*`) || 
        await req.user.can(`/${a}/*`)) {
      return next();
    }
    action.pop();
  }
  while (action.length > 0);

  return res.error(errors.UNAUTHORIZED);
};