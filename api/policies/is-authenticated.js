'use strict';

const User = require('../hooks/rbac/User');

module.exports = async function (req, res, next) {
  const errors = sails.config.errors;

  if(!req.headers || !req.headers.authorization) {
    return res.error(errors.UNAUTHORIZED)
  }

  const parts = req.headers.authorization.split(' ');
  if (parts.length !== 2) {
    return res.error(errors.UNAUTHORIZED)
  }

  if(!/^Bearer$/i.test(parts[0])) {
    return res.error(errors.UNAUTHORIZED)
  }

  const token = parts[1];

  sails.helpers.jwt.verify(token)
    .then(async decoded => {
      let t = await AccessToken.query().findOne({ token });
      if (!t) {
        return res.error(errors.TOKEN_INVALID);
      }
      if (t.expired) {
        return res.error(errors.TOKEN_EXPIRED);
      }
      if (t.revoked) {
        return res.error(errors.TOKEN_INVALID);
      }
      req.user = new User(token, decoded);
      return next();
    })
    .catch(async err => {
      sails.log.error(err);
      await AccessToken.query().where({ token }).update({ expired: true });
      return res.error(errors.TOKEN_INVALID);
    })
};