'use strict';

class UserPlan extends Model {
  static get tableName() {
    return 'user_plan';
  }
  
  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'user_plan.user_id',
          to: 'user.id'
        }
      },
      plan: {
        relation: Model.BelongsToOneRelation,
        modelClass: Plan,
        join: {
          from: 'user_plan.plan_id',
          to: 'plan.id'
        }
      }
    }
  }
}

module.exports = UserPlan;