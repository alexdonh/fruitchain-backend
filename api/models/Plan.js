'use strict';

class Plan extends Model {
  static get tableName() {
    return 'plan';
  }

  static get relationMappings() {
    return {
      translations: {
        relation: Model.HasManyRelation,
        modelClass: ProductTypeTranslation,
        join: {
          from: 'plan.id',
          to: 'plan_translation.plan_id'
        }
      },
      channels: {
        relation: Model.HasManyRelation,
        modelClass: Channel,
        join: {
          from: 'plan.id',
          to: 'channel.plan_id'
        }
      },
      userPlans: {
        relation: Model.HasManyRelation,
        modelClass: UserPlan,
        join: {
          from: 'plan.id',
          to: 'user_plan.plan_id'
        }
      }
    }
  }

  async translation(locale = 'en') {
    return this.$relatedQuery('translations').where({ locale }).first();
  }
}

module.exports = Plan;