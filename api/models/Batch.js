'use strict';

class Batch extends Model {
  static get tableName() {
    return 'batch';
  }

  static get relationMappings() {
    return {
      stickers: {
        relation: Model.HasManyRelation,
        modelClass: Sticker,
        join: {
          from: 'batch.id',
          to: 'sticker.batch_id'
        }
      },
      productType: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductType,
        join: {
          from: 'batch.product_type_id',
          to: 'product_type.id'
        }
      },
      productCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductCategory,
        join: {
          from: 'batch.product_category_id',
          to: 'product_category.id'
        }
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'batch.user_id',
          to: 'user.id'
        }
      }
    }
  }
}

module.exports = Batch;