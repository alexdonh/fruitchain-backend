'use strict';

class ProductTypeTranslation extends Model {
  static get tableName() {
    return 'product_type_translation';
  }

  static get idColumn() {
    return ['locale', 'product_type_id'];
  }

  static get relationMappings() {
    return {
      productType: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductType,
        join: {
          from: 'product_type_translation.product_type_id',
          to: 'product_type.id'
        }
      }
    }
  }
}

module.exports = ProductTypeTranslation;