'use strict';

class ProductType extends Model {
  static get tableName() {
    return 'product_type';
  }

  static get relationMappings() {
    return {
      productCategories: {
        relation: Model.HasManyRelation,
        modelClass: ProductCategory,
        join: {
          from: 'product_type.id',
          to: 'product_category.product_type_id'
        }
      },
      translations: {
        relation: Model.HasManyRelation,
        modelClass: ProductTypeTranslation,
        join: {
          from: 'product_type.id',
          to: 'product_type_translation.product_type_id'
        }
      }
    }
  }

  async translation(locale = 'en') {
    return this.$relatedQuery('translations').where({ locale }).first();
  }
}

module.exports = ProductType;