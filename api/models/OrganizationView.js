'use strict';

class OrganizationView extends Model {
  static get tableName() {
    return 'organization_view';
  }

  static get relationMappings() {
    return {
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: Organization,
        join: {
          from: 'organization_view.organization_id',
          to: 'organization.id'
        }
      }
    }
  }
}

module.exports = OrganizationView;