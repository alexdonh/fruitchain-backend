'use strict';

class StickerOrderItem extends Model {
  static get tableName() {
    return 'sticker_order_item';
  }

  static get relationMappings() {
    return {
      stickerOrder: {
        relation: Model.BelongsToOneRelation,
        modelClass: StickerOrder,
        join: {
          from: 'sticker_order_item.sticker_order_id',
          to: 'sticker_order.id'
        }
      },
      productType: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductType,
        join: {
          from: 'sticker_order_item.product_type_id',
          to: 'product_type.id'
        }
      }
    }
  }
}

module.exports = StickerOrderItem;