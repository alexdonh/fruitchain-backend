'use strict';

class UserChannel extends Model {
  static get tableName() {
    return 'user_channel';
  }

  static get idColumn() {
    return ['user_id', 'channel_id', 'product_type_id', 'product_category_id'];
  }
  
  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'user_channel.user_id',
          to: 'user.id'
        }
      },
      channel: {
        relation: Model.BelongsToOneRelation,
        modelClass: Channel,
        join: {
          from: 'user_channel.channel_id',
          to: 'channel.id'
        }
      }
    }
  }
}

module.exports = UserChannel;