'use strict';

const moment = require('moment');

class Certificate extends Model {
  static get tableName() {
    return 'certificate';
  }

  static get relationMappings() {
    return {
      organizations: {
        relation: Model.ManyToManyRelation,
        modelClass: Organization,
        join: {
          from: 'certificate.id',
          through: {
            from: 'organization_certificate.certificate_id',
            to: 'organization_certificate.organization_id',
          },
          to: 'organization.id'
        }
      }
    }
  }

  static get isValid() {
    return this.valid_at <= moment.utc() && this.expired_at >= moment.utc();
  }
}

module.exports = Certificate;