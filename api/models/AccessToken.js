'use strict';

class AccessToken extends Model {
  static get tableName() {
    return 'access_token';
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'access_token.user_id',
          to: 'user.id'
        }
      }
    }
  }
}
module.exports = AccessToken;