'use strict';

class AuthTranslation extends Model {
  static get tableName() {
    return 'auth_translation';
  }

  static get idColumn() {
    return ['locale', 'auth_id'];
  }

  static get relationMappings() {
    return {
      auth: {
        relation: Model.BelongsToOneRelation,
        modelClass: Auth,
        join: {
          from: 'auth_translation.auth_id',
          to: 'auth.id'
        }
      }
    }
  }
}

module.exports = AuthTranslation;