'use strict';

class Channel extends Model {
  static get tableName() {
    return 'channel';
  }

  static get relationMappings() {
    return {
      plan: {
        relation: Model.BelongsToOneRelation,
        modelClass: Plan,
        join: {
          from: 'channel.plan_id',
          to: 'plan.id'
        }
      },
      userChannels: {
        relation: Model.HasManyRelation,
        modelClass: UserChannel,
        join: {
          from: 'channel.id',
          to: 'user_channel.channel_id'
        }
      }
    }
  }
}

module.exports = Channel;