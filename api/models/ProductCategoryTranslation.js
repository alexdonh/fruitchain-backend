'use strict';

class ProductCategoryTranslation extends Model {
  static get tableName() {
    return 'product_category_translation';
  }

  static get idColumn() {
    return ['locale', 'product_category_id'];
  }

  static get relationMappings() {
    return {
      productCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductCategory,
        join: {
          from: 'product_category_translation.product_category_id',
          to: 'product_category.id'
        }
      }
    }
  }
}

module.exports = ProductCategoryTranslation;