'use strict';

class User extends Model {
  static get tableName() {
    return 'user';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['username', 'password', 'status'],
      properties: {
        username: { type: 'string', minLength: 6, maxLength: 64 },
        password: { type: 'string', minLength: 6, maxLength: 255 },
        status:   { type: 'integer' }
      }
    }
  }

  static get relationMappings() {
    return {
      accessTokens: {
        relation: Model.HasManyRelation,
        modelClass: AccessToken,
        join: {
          from: 'user.id',
          to: 'access_token.user_id'
        }
      },
      roles: {
        relation: Model.ManyToManyRelation,
        modelClass: Auth,
        join: {
          from: 'user.id',
          through: {
            from: 'user_auth.user_id',
            to: 'user_auth.auth_id',
          },
          to: 'auth.id'
        },
        filter: { type: Auth.TYPE_ROLE }
      },
      permissions: {
        relation: Model.ManyToManyRelation,
        modelClass: Auth,
        join: {
          from: 'user.id',
          through: {
            from: 'user_auth.user_id',
            to: 'user_auth.auth_id',
          },
          to: 'auth.id'
        },
        filter: { type: Auth.TYPE_PERMISSION }
      },
      productCategories: {
        relation: Model.ManyToManyRelation,
        modelClass: ProductCategory,
        join: {
          from: 'user.id',
          through: {
            from: 'user_product_category.user_id',
            to: 'user_product_category.product_category_id',
          },
          to: 'product_category.id'
        }
      },
      contact: {
        relation: Model.BelongsToOneRelation,
        modelClass: Contact,
        join: {
          from: 'user.id',
          to: 'contact.id'
        }
      },
      userPlans: {
        relation: Model.HasManyRelation,
        modelClass: UserPlan,
        join: {
          from: 'user.id',
          to: 'user_plan.user_id'
        }
      },
      userChannels: {
        relation: Model.HasManyRelation,
        modelClass: UserChannel,
        join: {
          from: 'user.id',
          to: 'user_channel.user_id'
        }
      }
    }
  }

  static get hidden() {
    return ['password', 'resetToken'];
  }

  async $beforeInsert(context) {
    await super.$beforeInsert(context);
    this.password = await sails.helpers.password.hash(this.password);
  }
}
module.exports = User;