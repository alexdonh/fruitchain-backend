'use strict';

class Auth extends Model {
  static get TYPE_ROLE() { return 1 }
  static get TYPE_PERMISSION() { return 2 }
  static get TYPE_RULE() { return 3 }
  
  static get tableName() {
    return 'auth';
  }

  static get relationMappings() {
    return {
      parents: {
        relation: Model.ManyToManyRelation,
        modelClass: Auth,
        join: {
          from: 'auth.id',
          through: {
            from: 'auth_child.child_id',
            to: 'auth_child.parent_id'
          },
          to: 'auth.id'
        }
      },
      children: {
        relation: Model.ManyToManyRelation,
        modelClass: Auth,
        join: {
          from: 'auth.id',
          through: {
            from: 'auth_child.parent_id',
            to: 'auth_child.child_id'
          },
          to: 'auth.id'
        }
      },
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'auth.id',
          through: {
            from: 'user_auth.auth_id',
            to: 'user_auth.user_id',
          },
          to: 'user.id'
        }
      },
      translations: {
        relation: Model.HasManyRelation,
        modelClass: AuthTranslation,
        join: {
          from: 'auth.id',
          to: 'auth_translation.auth_id'
        }
      },
    }
  }

  async translation(locale = 'en') {
    return this.$relatedQuery('translations').where({ locale }).first();
  }
}

module.exports = Auth;