'use strict';

class AuthChild extends Model {
  static get tableName() {
    return 'auth_child';
  }
  
  static get idColumn() {
    return ['parent_id', 'child_id'];
  }
}

module.exports = AuthChild;