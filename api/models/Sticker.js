'use strict';

class Sticker extends Model {
  static get tableName() {
    return 'sticker';
  }

  static get relationMappings() {
    return {
      parent: {
        relation: Model.BelongsToOneRelation,
        modelClass: Sticker,
        join: {
          from: 'sticker.parent_id',
          to: 'sticker.id'
        }
      },
      children: {
        relation: Model.HasManyRelation,
        modelClass: Sticker,
        join: {
          from: 'sticker.id',
          to: 'sticker.parent_id'
        }
      },
      owner: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'sticker.owner_id',
          to: 'user.id'
        }
      },
      stickerOrder: {
        relation: Model.BelongsToOneRelation,
        modelClass: StickerOrder,
        join: {
          from: 'sticker.sticker_order_id',
          to: 'sticker_order.id'
        }
      },
      stickerOrderItem: {
        relation: Model.BelongsToOneRelation,
        modelClass: StickerOrderItem,
        join: {
          from: 'sticker.sticker_order_item_id',
          to: 'sticker_order_item.id'
        }
      },
      statuses: {
        relation: Model.HasManyRelation,
        modelClass: StickerStatus,
        join: {
          from: 'sticker.id',
          to: 'sticker_status.sticker_id'
        }
      },
      batch: {
        relation: Model.BelongsToOneRelation,
        modelClass: Batch,
        join: {
          from: 'sticker.batch_id',
          to: 'batch.id'
        }
      },
      channel: {
        relation: Model.BelongsToOneRelation,
        modelClass: Channel,
        join: {
          from: 'sticker.channel_id',
          to: 'channel.id'
        }
      }
    }
  }
}

module.exports = Sticker;