'use strict';

class DeliveryOrder extends Model {
  static get tableName() {
    return 'delivery_order';
  }

  static get relationMappings() {
    return {
      deliveryOrderItems: {
        relation: Model.HasManyRelation,
        modelClass: DeliveryOrderItem,
        join: {
          from: 'delivery_order.id',
          to: 'delivery_order_item.delivery_order_id'
        }
      },
      transferor: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'delivery_order.transferor_id',
          to: 'user.id'
        }
      },
      transferee: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'delivery_order.transferee_id',
          to: 'user.id'
        }
      },
      courier: {
        relation: Model.BelongsToOneRelation,
        modelClass: Courier,
        join: {
          from: 'delivery_order.courier_id',
          to: 'courier.id'
        }
      }
    }
  }
}

module.exports = DeliveryOrder;