'use strict';

class ProductCategory extends Model {
  static get tableName() {
    return 'product_category';
  }

  static get relationMappings() {
    return {
      productType: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductType,
        join: {
          from: 'product_category.product_type_id',
          to: 'product_type.id'
        }
      },
      translations: {
        relation: Model.HasManyRelation,
        modelClass: ProductTypeTranslation,
        join: {
          from: 'product_type.id',
          to: 'product_type_translation.product_type_id'
        }
      }
    }
  }

  async translation(locale = 'en') {
    return this.$relatedQuery('translations').where({ locale }).first();
  }
}

module.exports = ProductCategory;