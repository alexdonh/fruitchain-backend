'use strict';

class Courier extends Model {
  static get tableName() {
    return 'courier';
  }

  static get relationMappings() {
    return {
      deliveryOrders: {
        relation: Model.HasManyRelation,
        modelClass: DeliveryOrder,
        join: {
          from: 'courier.id',
          to: 'delivery_order.courier_id'
        }
      }
    }
  }
}

module.exports = Courier;