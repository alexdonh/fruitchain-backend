'use strict';

const moment = require('moment');

class Organization extends Model {
  static get tableName() {
    return 'organization';
  }

  static get relationMappings() {
    return {
      contacts: {
        relation: Model.HasManyRelation,
        modelClass: Contact,
        join: {
          from: 'organization.id',
          to: 'contact.organization_id'
        }
      },
      certificates: {
        relation: Model.ManyToManyRelation,
        modelClass: Certificate,
        join: {
          from: 'organization.id',
          through: {
            from: 'organization_certificate.organization_id',
            to: 'organization_certificate.certificate_id',
          },
          to: 'certificate.id'
        }
      },
      validCertificates: {
        relation: Model.ManyToManyRelation,
        modelClass: Certificate,
        join: {
          from: 'organization.id',
          through: {
            from: 'organization_certificate.organization_id',
            to: 'organization_certificate.certificate_id',
          },
          to: 'certificate.id'
        },
        filter: (query) => {
          query
            .where('valid_at', '<=', moment.utc())
            .andWhere('expired_at', '>=', moment.utc());
        }
      }
    }
  }
}

module.exports = Organization;