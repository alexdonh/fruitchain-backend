'use strict';

class StickerOrder extends Model {
  static get tableName() {
    return 'sticker_order';
  }

  static get relationMappings() {
    return {
      stickerOrderItems: {
        relation: Model.HasManyRelation,
        modelClass: StickerOrderItem,
        join: {
          from: 'sticker_order.id',
          to: 'sticker_order_item.sticker_order_id'
        }
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'sticker_order.user_id',
          to: 'user.id'
        }
      }
    }
  }
}

module.exports = StickerOrder;