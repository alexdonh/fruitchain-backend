'use strict';

class Contact extends Model {
  static get tableName() {
    return 'contact';
  }

  static get relationMappings() {
    return {
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: Organization,
        join: {
          from: 'contact.organization_id',
          to: 'organization.id'
        }
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'contact.id',
          to: 'user.id'
        }
      }
    }
  }
}

module.exports = Contact;