'use strict';

class DeliveryOrderItem extends Model {
  static get tableName() {
    return 'delivery_order_item';
  }

  static get relationMappings() {
    return {
      deliveryOrder: {
        relation: Model.BelongsToOneRelation,
        modelClass: DeliveryOrder,
        join: {
          from: 'delivery_order_item.delivery_order_id',
          to: 'delivery_order.id'
        }
      },
      productType: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductType,
        join: {
          from: 'delivery_order_item.product_type_id',
          to: 'product_type.id'
        }
      },
      productCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProductCategory,
        join: {
          from: 'delivery_order_item.product_category_id',
          to: 'product_category.id'
        }
      },
      stickers: {
        relation: Model.ManyToManyRelation,
        modelClass: Sticker,
        join: {
          from: 'delivery_order_item.id',
          through: {
            from: 'delivery_order_item_sticker.delivery_order_item_id',
            to: 'delivery_order_item_sticker.sticker_id',
          },
          to: 'sticker.id'
        }
      }
    }
  }
}

module.exports = DeliveryOrderItem;