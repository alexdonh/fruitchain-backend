'use strict';

class UserAuth extends Model {
  static get tableName() {
    return 'user_auth';
  }
  
  static get idColumn() {
    return ['user_id', 'auth_id'];
  }
}

module.exports = UserAuth;