'use strict';

class StickerView extends Model {
  static get tableName() {
    return 'sticker_view';
  }

  static get relationMappings() {
    return {
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: Sticker,
        join: {
          from: 'sticker_view.organization_id',
          to: 'sticker.id'
        }
      }
    }
  }
}

module.exports = StickerView;