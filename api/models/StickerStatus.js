'use strict';

class StickerStatus extends Model {
  static get tableName() {
    return 'sticker_status';
  }

  static get relationMappings() {
    return {
      sticker: {
        relation: Model.BelongsToOneRelation,
        modelClass: Sticker,
        join: {
          from: 'sticker_status.sticker_id',
          to: 'sticker.id'
        }
      },
      transferor: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'sticker_status.transferor_id',
          to: 'user.id'
        }
      },
      transferee: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'sticker_status.transferee_id',
          to: 'user.id'
        }
      }
    }
  }
}

module.exports = StickerStatus;