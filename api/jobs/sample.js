'use strict';

module.exports = {
  //worker concurrency
  concurrency: 2,

  fn: function(job, context, done) {
    sails.log.info(`Job ${job.id} of type ${job.type} is executed.`);
    done();
  }
};