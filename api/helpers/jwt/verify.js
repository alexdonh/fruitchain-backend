'use strict';

/**
 * JWT helper
 */

const jwt = require('jsonwebtoken');

module.exports = {
  friendlyName: 'Verify',
  description: 'Verify JWT token.',
  inputs: {
    token: { type: 'string', required: true }
  },
  fn: async function (inputs, exits) {
    jwt.verify(inputs.token, sails.config.custom.secret, { algorithm: 'HS256', clockTolerance: 10 }, function(err, decoded) {
      if (err) {
        return exits.error(err);
      }
      return exits.success(decoded)
    });
  }
};

