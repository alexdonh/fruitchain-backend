'use strict';

/**
 * JWT helper
 */

const jwt = require('jsonwebtoken');

module.exports = {
  friendlyName: 'Sign',
  description: 'Sign a payload and generate JWT token.',
  inputs: {
    payload: { type: 'ref', required: true }
  },
  fn: async function (inputs, exits) {
    jwt.sign(inputs.payload, sails.config.custom.secret, { algorithm: 'HS256', expiresIn: 86400 }, function(err, token) {
      if (err) {
        return exits.error(err);
      }
      return exits.success(token);
    });
  }
};

