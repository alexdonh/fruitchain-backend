'use strict';

/**
 * Sawtooth batch helper
 */

const _ = require('@sailshq/lodash');
const { protobuf } = require('sawtooth-sdk');
const { createContext, CryptoFactory } = require('sawtooth-sdk/signing');
const { Secp256k1PrivateKey } = require('sawtooth-sdk/signing/secp256k1');

function signer(privateKey) {
  return new CryptoFactory(createContext('secp256k1')).newSigner(Secp256k1PrivateKey.fromHex(privateKey));
}

module.exports = {
  friendlyName: 'Create sawtooth batch',
  description: '',
  inputs: {
    transactions: { type: 'ref', description: 'Transactions', required: true }
  },
  fn: function (inputs, exits) {
    if (!_.isArray(inputs.transactions)) {
      inputs.transactions = [input.transactions];
    }
    try {
      const signer = signer(sails.config.sawtooth.privateKey);

      const batchHeaderBytes = protobuf.BatchHeader.encode({
        signerPublicKey: signer.getPublicKey().asHex(),
        transactionIds: inputs.transactions.map((t) => t.headerSignature)
      }).finish();
  
      const batchSignature = signer.sign(batchHeaderBytes);
  
      const batchBytes = protobuf.Batch.create({
        header: batchHeaderBytes,
        headerSignature: batchSignature,
        transactions: inputs.transactions
      });

      return exits.success(batchBytes);
    } catch(err) {
      sails.log.error(err);
      return exits.success(false);
    }
  }
};

