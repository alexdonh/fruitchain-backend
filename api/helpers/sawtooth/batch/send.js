'use strict';

/**
 * Sawtooth batch helper
 */

const _ = require('@sailshq/lodash');
const rp = require('request-promise');
const { protobuf } = require('sawtooth-sdk')

module.exports = {
  friendlyName: 'Send sawtooth batch',
  description: '',
  inputs: {
    endpoint: { type: 'string', description: 'Sawtooth endpoint', required: true },
    batches: { type: 'ref', description: 'Batches', required: true }
  },
  fn: async function (inputs, exits) {
    if (!_.isArray(inputs.batches)) {
      inputs.batches = [inputs.batches];
    }

    const batchListBytes = protobuf.BatchList.encode({
      batches: batches
    }).finish();

    return exits.success(rp({
      method: 'POST',
      url: `${inputs.endpoint}/batches`,
      body: batchListBytes,
      headers: { 'Content-Type': 'application/octet-stream' }
    }));
  }
};

