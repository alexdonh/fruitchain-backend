'use strict';

/**
 * Sawtooth helper
 */

function stringToBytes(str) {
  let arr = [];
  for (let i = 0; i < str.length; ++i) {
    arr = arr.concat([str.charCodeAt(i)]);
  }
  return arr;
}

function bytesToHex(buffer) {
  let arr = [];
  for (let i = 0; i < buffer.length; ++i) {
    arr = arr.concat([buffer[i].toString(16)]);
  }
  return arr.join('').toString();
}

function sha512(str) {
  const crypto = require('crypto');
  return crypto.createHash('sha512').update(str).digest('hex');
}

module.exports = {
  friendlyName: 'Hexdigest',
  description: '',
  inputs: {
    data: { type: 'ref', description: 'Data', required: true }
  },
  fn: function (inputs, exits) {
    const arr = stringToBytes(inputs.data);
    const str = bytesToHex(sha512(Buffer.from(arr)));
    return exits.success(str);
  }
};

