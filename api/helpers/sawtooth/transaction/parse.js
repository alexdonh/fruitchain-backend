'use strict';

/**
 * Sawtooth transaction helper
 */

module.exports = {
  friendlyName: 'Parse transaction',
  description: '',
  inputs: {
    data: { type: 'ref', description: 'Data', required: true }
  },
  fn: async function (inputs, exits) {
    try {
      return exits.success(protobuf.Transaction.decode(
        Buffer.from(
          JSON.parse(
            inputs.data
          )
        )
      ));
    } catch (err) {
      sails.log.error(err);
      return exits.success(false);
    }
  }
};

