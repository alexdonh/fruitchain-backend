'use strict';

/**
 * Sawtooth transaction helper
 */

const { protobuf } = require('sawtooth-sdk');
const { createContext, CryptoFactory } = require('sawtooth-sdk/signing');
const { Secp256k1PrivateKey } = require('sawtooth-sdk/signing/secp256k1');
const {
  TransactionHeader,
  Transaction
} = require('../../../../config/sawtooth/index');

function signer(privateKey) {
  return new CryptoFactory(createContext('secp256k1')).newSigner(Secp256k1PrivateKey.fromHex(privateKey));
}

function sha512(str) {
  const crypto = require('crypto');
  return crypto.createHash('sha512').update(str).digest('hex');
}

module.exports = {
  friendlyName: 'Create transaction',
  description: '',
  inputs: {
    input:   { type: 'ref', description: 'Input', required: true },
    output:  { type: 'ref', description: 'Output', required: true },
    payload: { type: 'ref', description: 'Payload bytes', require: true },
    dependencies: { type: 'ref', description: 'Dependencies', defaultsTo: [] }
  },
  fn: function (inputs, exits) {
    try {
      const signer = signer(sails.config.sawtooth.privateKey);
      const publicKey = signer.getPublicKey().asHex();
      const transactionHeaderBytes = TransactionHeader.encode({
        familyName: sails.config.sawtooth.familyName,
        familyVersion: sails.config.sawtooth.familyVersion,
        inputs: inputs.input,
        outputs: inputs.output,
        signerPublicKey: publicKey,
        batcherPublicKey: publicKey,
        dependencies: inputs.dependencies,
        payloadSha512: sha512(inputs.payload)
      }).finish();
      
      const signature = signer.sign(transactionHeaderBytes);
      const transaction = Transaction.create({
        header: transactionHeaderBytes,
        headerSignature: signature,
        payload: inputs.payload
      });
  
      return exits.success(transaction);
    } catch(err) {
      sails.log.error(err);
      return exits.success(false);
    }
  }
};

