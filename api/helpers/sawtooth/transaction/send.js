'use strict';

/**
 * Sawtooth transaction helper
 */

module.exports = {
  friendlyName: 'Send transactions to sawtooth',
  description: '',
  inputs: {
    endpoint: { type: 'string', description: 'Sawtooth endpoint', required: true },
    transactions: { type: 'ref', description: 'Transactions', required: true }
  },
  fn: async function (inputs, exits) {
    const batch = sails.helpers.batch.create(inputs.transactions);
    return exits.success(sails.helpers.batch.send(inputs.endpoint, batch));
  }
};

