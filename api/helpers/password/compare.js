'use strict';

/**
 * Password helper
 */

const bcrypt = require('bcrypt');

module.exports = {
  friendlyName: 'Compare password',
  description: 'Compare a plaintext password attempt against a Bcrypt hash (i.e. already-encrypted).',
  inputs: {
    password: { type: 'string', required: true },
    hash: { type: 'string', required: true }
  },
  fn: async function (inputs, exits) {
    bcrypt.compare(inputs.password, inputs.hash, function(err, ok) {

      // Forward any errors to our `error` exit.
      if (err) {
        return exits.error(err);
      }

      // Otherwise return through the `success` exit.
      return exits.success(ok);

    });
  }
};

