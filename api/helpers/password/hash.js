'use strict';

/**
 * Password helper
 */

const bcrypt = require('bcrypt');

module.exports = {
  friendlyName: 'Hash password',
  description: 'Hash a password (i.e. one-way encryption) using the BCrypt algorithm.',
  inputs: {
    password: { type: 'string', description: 'The password to hash (in plain text).', required: true },
    strength: { type: 'number', description: 'The hash strength.', defaultsTo: 10 }
  },
  fn: async function (inputs, exits) {
    bcrypt.hash(inputs.password, inputs.strength, function(err, hashed) {

      // Forward any errors to our `error` exit.
      if (err) {
        return exits.error(err);
      }

      // Return the hashed password through the `success` exit.
      return exits.success(hashed);

    });
  }
};

