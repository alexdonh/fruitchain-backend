'use strict';

/**
 * error.js
 *
 * A custom response.
 *
 * Example usage:
 * ```
 *     return res.serverError();
 *     // -or-
 *     return res.serverError(optionalData);
 * ```
 *
 * Or with actions2:
 * ```
 *     exits: {
 *       somethingHappened: {
 *         responseType: 'serverError'
 *       }
 *     }
 * ```
 *
 * ```
 *     throw 'somethingHappened';
 *     // -or-
 *     throw { somethingHappened: optionalData }
 * ```
 */
module.exports = function error(err) {
  const errors = sails.config.errors;
  // if (!_.isArray(err)) {
  //   err = [err];
  // }
  return this.res
    .status(
      _.isObject(err) &&
      err.hasOwnProperty('code') && 
      errors.__MAPPING__.hasOwnProperty(err.code) 
      ? errors.__MAPPING__[err.code]
      : 500)
    .json(err);
}