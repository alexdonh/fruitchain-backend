'use strict';

const { Model, snakeCaseMappers } = require('objection');
const moment = require('moment');
const knex = require('knex');
const Visibility = require('objection-visibility').default;
const Knexify = require('./Knexify');
const Paginate = require('./Paginate');
const Validator = require('./Validator');

module.exports = class extends Paginate(Knexify(Visibility(Model), knex(sails.config.knex))) {
  static get columnNameMappers() {
    return snakeCaseMappers();
  }
  
  static get useLimitInFirst() {
    return true;
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeUpdate(opt, queryContext);
    this.updatedAt = moment().toISOString();
  }

  static createValidator() {
    return new Validator({
      onCreateAjv: (ajv) => {
        // Here you can modify the `Ajv` instance.
      },
      options: {
        allErrors: true,
        jsonPointers: true,
      }
    });
  }
};