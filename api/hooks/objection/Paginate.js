'use strict';

module.exports = function(Model) {
  class PaginateQueryBuilder extends Model.QueryBuilder {
    async paginate(page = 1, perPage = 10) {
      if (isNaN(page)) {
        throw new Error('page must be an number.');
      }
      if (isNaN(perPage)) {
        throw new Error('perPage must be a number.');
      }

      page = parseInt(page);

      if (page < 1) {
        page = 1;
      }

      const offset = (page - 1) * perPage;

      let promises = [];

      promises.push(this.clone().clearSelect().clearOrder().count('* as total').first());
      promises.push(this.offset(offset).limit(perPage));

      return Promise.all(promises).then(([countQuery, result]) => {
        const total = parseInt(countQuery.total);
        return {
          total: total,
          pages: Math.ceil(total / perPage),
          page: page,
          perPage: perPage,
          items: result
        };
      });
    }
  }
  return class extends Model {
    static get QueryBuilder() {
      return PaginateQueryBuilder;
    }
  };
}