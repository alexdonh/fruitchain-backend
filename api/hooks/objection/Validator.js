'use strict';

String.prototype.strtr = function (dic) { 
  const str = this.toString(),
        makeToken = (inx) => `{{###~${inx}~###}}`,
        tokens = Object.keys( dic )       
          .map((key, inx) => ({
            key,
            val: dic[key],
            token: makeToken(inx)
          })),
        tokenizedStr = tokens.reduce((carry, entry) => carry.replace(entry.key, entry.token), str);
  return tokens.reduce((carry, entry) => carry.replace(entry.token, entry.val), tokenizedStr);
};

const { Validator } = require('objection/lib/model/Validator');
const { ValidationErrorType } = require('objection/lib/model/ValidationError');
const { isObject, once, cloneDeep: lodashCloneDeep, omit } = require('objection/lib/utils/objectUtils');

const defaultValidationMessages = {
  required:         '{property} cannot be left blank.',
  number:           '{property} must be a number.',
  integer:          '{property} must be an integer.',
  string:           '{property} must be a string.',
  boolean:          '{property} must be either "{true}" or "{false}".',
  array:            '{property} must be an array.',
  object:           '{property} must be an object.',
  maximum:          '{property} must be no greater than {max}.',
  minimum:          '{property} must be no less than {min}.',
  exclusiveMaximum: '{property} must be no greater than or equal {max}.',
  exclusiveMinimum: '{property} must be no less than or equal {min}.',
  maxLength:        '{property} should contain at most {max} characters.',
  minLength:        '{property} should contain at least {min} characters.',
  pattern:          '{property} is invalid.',
  format:           '{property} must be in {format} format.',
  maxItems:         '{property} must have no more than {max} items.',
  minItems:         '{property} must have no less than {min} items.',
  uniqueItems:      '{property} must have unique items.',
  maxProperties:    '{property} must have no more than {max} properties.',
  minProperties:    '{property} must have no less than {max} properties.',
  enum:             '{property} must be one of the following values: {values}.',
  const:            '{property} must be exactly {value}.'
}

const getAjv = once(() => {
  try {
    return require('ajv');
  } catch (err) {
    throw new Error('Optional ajv dependency not installed. Please run `npm install ajv --save`');
  }
});

function parseValidationError(errors, modelClass, options) {
  if (!errors) {
    return null;
  }

  let relationNames = modelClass.getRelationNames();
  let errorHash = {};
  let numErrors = 0;

  for (const error of errors) {
    const dataPath = `${options.dataPath || ''}${error.dataPath}`;

    // If additionalProperties = false, relations can pop up as additionalProperty
    // errors. Skip those.
    if (
      error.params &&
      error.params.additionalProperty &&
      relationNames.includes(error.params.additionalProperty)
    ) {
      continue;
    }

    // Unknown properties are reported in `['propertyName']` notation,
    // so replace those with dot-notation, see:
    // https://github.com/epoberezkin/ajv/issues/671
    const key = dataPath.replace(/\['([^' ]*)'\]/g, '.$1').substring(1);

    // More than one error can occur for the same key in Ajv, merge them in the array:
    const array = errorHash[key] || (errorHash[key] = []);

    const jsonSchema = modelClass.getJsonSchema();

    // Use unshift instead of push so that the last error ends up at [0],
    // preserving previous behavior where only the last error was stored.
    array.unshift(parseValidationErrorMessage(key, error, jsonSchema));

    ++numErrors;
  }

  if (numErrors === 0) {
    return null;
  }

  return modelClass.createValidationError({
    type: ValidationErrorType.ModelValidation,
    data: errorHash
  });
}

function parseValidationErrorMessage(key, error, jsonSchema) {
  let message =   jsonSchema.hasOwnProperty('messages') && 
                  jsonSchema.messages.hasOwnProperty(key) &&
                  jsonSchema.messages[key].hasOwnProperty(error.keyword)
                  ? jsonSchema.messages[key][error.keyword]
                  : defaultValidationMessages[error.keyword] || error.message;
  
  switch(error.keyword) {
    case 'boolean':
      message = message.strtr({'{true}': 'true', '{false}': 'false'});
      break;
    case 'maximum':
      if (message === defaultValidationMessages['maximum'] && error.params.exclusive) {
        message = defaultValidationMessages['exclusiveMaximum'];
      }
      message = message.strtr({'{max}': error.params.limit});
      break;
    case 'minimum':
      if (message === defaultValidationMessages['minimum'] && error.params.exclusive) {
        message = defaultValidationMessages['exclusiveMinimum'];
      }
      message = message.strtr({'{min}': error.params.limit});
      break;
    case 'maxLength':
    case 'maxItems':
    case 'maxProperties':
      message = message.strtr({'{max}': error.params.limit});
      break;
    case 'minLength':
    case 'minItems':
    case 'minProperties':
      message = message.strtr({'{min}': error.params.limit});
      break;
    case 'format':
      message = message.strtr({'{format}': error.params.format});
      break;
    case 'enum':
      message = message.strtr({'{values}': JSON.stringify(error.params.allowedValues)});
      break;
    case 'const':
      message = message.strtr({'{value}': JSON.stringify(error.params.allowedValue)});
      break;
  }
  return message.strtr({'{property}': key });
}

function cloneDeep(obj) {
  if (isObject(obj) && obj.$isObjectionModel) {
    return obj.$clone();
  } else {
    return lodashCloneDeep(obj);
  }
}

function setsDefaultValues(jsonSchema) {
  return jsonSchema && jsonSchema.properties && hasDefaults(jsonSchema.properties);
}

function hasDefaults(obj) {
  if (Array.isArray(obj)) {
    return arrayHasDefaults(obj);
  } else {
    return objectHasDefaults(obj);
  }
}

function arrayHasDefaults(arr) {
  for (let i = 0, l = arr.length; i < l; ++i) {
    const val = arr[i];

    if (isObject(val) && hasDefaults(val)) {
      return true;
    }
  }

  return false;
}

function objectHasDefaults(obj) {
  const keys = Object.keys(obj);

  for (let i = 0, l = keys.length; i < l; ++i) {
    const key = keys[i];

    if (key === 'default') {
      return true;
    } else {
      const val = obj[key];

      if (isObject(val) && hasDefaults(val)) {
        return true;
      }
    }
  }

  return false;
}

function jsonSchemaWithoutRequired(jsonSchema) {
  const subSchemaProps = ['anyOf', 'oneOf', 'allOf', 'not', 'then', 'else'];

  return Object.assign(
    omit(jsonSchema, ['required', ...subSchemaProps]),
    ...subSchemaProps.map(prop => subSchemaWithoutRequired(jsonSchema, prop))
  );
}

function subSchemaWithoutRequired(jsonSchema, prop) {
  if (jsonSchema[prop]) {
    if (Array.isArray(jsonSchema[prop])) {
      const schemaArray = jsonSchemaArrayWithoutRequired(jsonSchema[prop]);

      if (schemaArray.length !== 0) {
        return {
          [prop]: schemaArray
        };
      } else {
        return {};
      }
    } else {
      return {
        [prop]: jsonSchemaWithoutRequired(jsonSchema[prop])
      };
    }
  } else {
    return {};
  }
}

function jsonSchemaArrayWithoutRequired(jsonSchemaArray) {
  return jsonSchemaArray.map(jsonSchemaWithoutRequired).filter(isNotEmptyObject);
}

function isNotEmptyObject(obj) {
  return Object.keys(obj).length !== 0;
}

module.exports = class extends Validator {
  static init(self, conf) {
    super.init(self, conf);

    self.ajvOptions = Object.assign({ errorDataPath: 'property' }, conf.options, {
      allErrors: true
    });

    // Create a normal Ajv instance.
    self.ajv = new getAjv()(
      Object.assign(
        {
          useDefaults: true
        },
        self.ajvOptions
      )
    );

    // Create an instance that doesn't set default values. We need this one
    // to validate `patch` objects (objects that have a subset of properties).
    self.ajvNoDefaults = new getAjv()(
      Object.assign({}, self.ajvOptions, {
        useDefaults: false
      })
    );

    // A cache for the compiled validator functions.
    self.cache = new Map();

    conf.onCreateAjv(self.ajv);
    conf.onCreateAjv(self.ajvNoDefaults);
  }

  beforeValidate({ json, model, options, ctx }) {
    ctx.jsonSchema = model.constructor.getJsonSchema();

    // Objection model's have a `$beforeValidate` hook that is allowed to modify the schema.
    // We need to clone the schema in case the function modifies it. We only do this in the
    // rare case that the given model has implemented the hook.
    if (model.$beforeValidate !== model.$objectionModelClass.prototype.$beforeValidate) {
      ctx.jsonSchema = cloneDeep(ctx.jsonSchema);
      const ret = model.$beforeValidate(ctx.jsonSchema, json, options);

      if (ret !== undefined) {
        ctx.jsonSchema = ret;
      }
    }
  }

  validate({ json, model, options, ctx }) {
    if (!ctx.jsonSchema) {
      return json;
    }

    const modelClass = model.constructor;
    const validator = this.getValidator(modelClass, ctx.jsonSchema, !!options.patch);

    // We need to clone the input json if we are about to set default values.
    if (!options.mutable && !options.patch && setsDefaultValues(ctx.jsonSchema)) {
      json = cloneDeep(json);
    }

    validator.call(model, json);
    const error = parseValidationError(validator.errors, modelClass, options);

    if (error) {
      throw error;
    }

    return json;
  }

  getValidator(modelClass, jsonSchema, isPatchObject) {
    // Use the AJV custom serializer if provided.
    const createCacheKey = this.ajvOptions.serialize || JSON.stringify;

    // Optimization for the common case where jsonSchema is never modified.
    // In that case we don't need to call the costly createCacheKey function.
    const cacheKey =
      jsonSchema === modelClass.getJsonSchema()
        ? modelClass.uniqueTag()
        : createCacheKey(jsonSchema);

    let validators = this.cache.get(cacheKey);
    let validator = null;

    if (!validators) {
      validators = {
        // Validator created for the schema object without `required` properties
        // using the AJV instance that doesn't set default values.
        patchValidator: null,

        // Validator created for the unmodified schema.
        normalValidator: null
      };

      this.cache.set(cacheKey, validators);
    }

    if (isPatchObject) {
      validator = validators.patchValidator;

      if (!validator) {
        validator = this.compilePatchValidator(jsonSchema);
        validators.patchValidator = validator;
      }
    } else {
      validator = validators.normalValidator;

      if (!validator) {
        validator = this.compileNormalValidator(jsonSchema);
        validators.normalValidator = validator;
      }
    }

    return validator;
  }

  compilePatchValidator(jsonSchema) {
    jsonSchema = jsonSchemaWithoutRequired(jsonSchema);
    // We need to use the ajv instance that doesn't set the default values.
    return this.ajvNoDefaults.compile(jsonSchema);
  }

  compileNormalValidator(jsonSchema) {
    return this.ajv.compile(jsonSchema);
  }
};