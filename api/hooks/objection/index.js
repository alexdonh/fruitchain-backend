/**
 * objection.js hook
 *
 * @description :: A hook to replace Waterline ORM by Objection.js (and Knex)
 * @docs        :: https://vincit.github.io/objection.js
 * @docs        :: https://knexjs.org
 */

const async = require('async');

module.exports = function (sails) {

  return {

    defaults: {
      __configKey__: {
        active: true
      }
    },

    /**
     * configure()
     */
    configure: function() {
      if (!sails.hooks.objection.models) {
        sails.hooks.objection.models = {};
        // Expose a reference to `hook.models` as `sails.models`
        sails.models = sails.hooks.objection.models;
      }

      // Listen for reload events.
      sails.on('hook:objection:reload', sails.hooks.objection.reload);

      // Listen for lower event, and tear down all of the adapters
      sails.once('lower', sails.hooks.objection.teardown);

    },

    /**
     * Runs when this Sails app loads/lifts.
     */
    initialize: async function(done) {

      const config = sails.config[this.configKey];
      
      if (!config.active) {
        return done();
      }

      async.auto({
        _loadModels: function(next) {
          sails.log.silly('Loading app models...');

          global['Model'] = require('./Model');

          sails.modules.loadModels(function (err, modelDefs) {

            if (err) {
              if (err.code === 'include-all:DUPLICATE' && err.duplicateIdentity) {
                return next(new Error(
                                '\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n'+
                                'Attempted to load two models with the same identity (`' + err.duplicateIdentity + '`).  Please rename one of the files.\n'+
                                'The model identity is the lower-cased version of the filename.\n'+
                                '-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n'));
              }
              return next(err);
            }

            // Update the dictionary of models stored on our hook (`sails.hooks.objection.models`).
            // Note that the reference on the app instance (`sails.models`) is just an alias of this.
            _.extend(sails.hooks.objection.models, modelDefs);

            // If configured to do so (based on `sails.config.globals.models`), then expose a reference
            // to this model as a global variable (based on its `globalId`).
            if (_.isObject(sails.config.globals) && sails.config.globals.models === true) {
              _.each(sails.hooks.objection.models, function(model, modelIdentity) {
                // Use the `globalId` to determine what to globalize it as.
                if (_.isString(sails.hooks.objection.models[modelIdentity].globalId) && sails.hooks.objection.models[modelIdentity].globalId !== '') {
                  global[sails.hooks.objection.models[modelIdentity].globalId] = model;
                }
                // If there is no `globalId`, fall back to the identity.
                else {
                  global[modelIdentity] = model;
                }
              })
            }

            return next();
          });
        },

        _initializeDatabase: ['_loadModels', function(_, next) {
          if (!Model.knex().client.pool) {
            Model.knex().initialize();
          }
          return next();
        }]
        
      }, function(err, result) {
        if (err) {
          throw err;
        }
        sails.emit('hook:objection:loaded');
        return done();
      });
    },

    /**
     * sails.hooks.objection.reload()
     */
    reload: function (done) {
      done = done || function (err) {
        if (err) {
          sails.log.error('Failed to reload ORM hook.  Details:',err);
        }
      };

      // Teardown all of the adapters, since `.initialize()` will restart them.
      sails.hooks.objection.teardown(function(err) {
        if (err) { return done(err); }

        // Now run `.initialize()` again.
        sails.hooks.objection.initialize(function(err) {
          if (err) {
            var contextualErr = new Error('Failed to reinitialize ORM because the `initialize()` method of the ORM hook returned an error.  \nDetails:\n'+err.stack);
            return done(contextualErr);
          }

          // If the re-initialization was a success, trigger an event in case something
          // needs to respond to the ORM reload (e.g. pubsub hook).
          // Note that, since now there is an optional callback, this event may be deprecated
          // in future versions of Sails.
          sails.emit('hook:objection:reloaded');

          return done();
        });
      });
    },


    /**
     * sails.hooks.objection.teardown()
     *
     * Tear down the ORM.
     */
    teardown: function (done) {
      
      // Normalize optional callback.
      if (_.isUndefined(done)) {
        done = function (err){
          if (err) {
            sails.log.error('Could not tear down the ORM hook.  Error details:', err);
            sails.log.verbose('(The error above was logged like this because `sails.hooks.objection.teardown()` encountered an error in a code path where it was invoked without providing a callback.)');
            return;
          }//-•
        };
      }
      else if (!_.isFunction(done)) {
        throw new Error('Consistency violation: If specified, `done` must be a function.');
      }

      // If the ORM hasn't been built yet, then don't worry about tearing it down.
      if (!global['Model'] || !Model.knex()) {
        return done();
      }//-•

      // Tear down the ORM.
      try {
        Model.knex().destroy(function (err) {
          if (err) { return done(err); }
          else { return done(); }
        });
      } catch (e) { return done(e); }

    },

  };

};
