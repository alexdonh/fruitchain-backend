'use strict';

module.exports = function(Model, knex) {
  Model.knex(knex);
  return Model;
}