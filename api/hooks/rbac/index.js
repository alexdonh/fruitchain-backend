'use strict';

const _ = require('lodash');
const Manager = require('./Manager');

/**
 * rbac hook
 *
 * @description :: A hook definition.  Extends Sails by adding shadow routes, implicit actions, and/or initialization logic.
 * @docs        :: https://sailsjs.com/docs/concepts/extending-sails/hooks
 */

module.exports = function (sails) {

  return {

    defaults: {
      __configKey__: {
        active: true,
        defaultRoles: []
      }
    },

    configure: function() {
      const config = sails.config[this.configKey];

      if (_.isFunction(config.defaultRoles)) {
        config.defaultRoles = config.defaultRoles.call(this);
      }
      else if (_.isArray(config.defaultRoles)) {
        config.defaultRoles = config.defaultRoles;
      }
    },

    /**
     * Runs when this Sails app loads/lifts.
     */
    initialize: async function(done) {

      const config = sails.config[this.configKey];
      
      if (!config.active) {
        return done();
      }

      // must wait until cache & objection hook is loaded
      sails.after(['hook:cache:loaded', 'hook:objection:loaded'], async () => {

        this.manager = new Manager(config);
        sails.rbac = this;

        // const test = await this.manager.checkAccess(8, 'GET /default/index');
        // sails.log.info(test);
        
        sails.emit('hook:rbac:loaded');
        return done();
      })
    }
  }

};
