'use strict';

const _ = require('lodash');

module.exports = class Manager {

  constructor(config) {
    this.cache                   = config.cache || sails.cache;
    this.cacheKey                = config.cacheKey || 'rbac';
    this.items                   = {};
    this.parents                 = {};
    this._checkAccessAssignments = {};
    this._defaultRoles           = [];
  }

  get defaultRoles() {
    return this._defaultRoles;
  }

  set defaultRoles(roles) {
    if (!roles) {
      return;
    }
    if (_.isArray(roles)) {
      this._defaultRoles = roles;
    }
    else if (_.isFunction(roles)) {
      roles = roles.call(this);
      if (!_.isArray(this._defaultRoles)) {
        throw new Error('Default roles callable must return an array');
      }
      this._defaultRoles = roles;
    }
    else {
      throw new Error('Default roles must be either an array or a callable');
    }
  }

  async checkAccess(userId, authId, params = {}) {
    let assignments = null;
    if (this._checkAccessAssignments.hasOwnProperty(userId)) {
      assignments = this._checkAccessAssignments[userId];
    } else {
      assignments = await this.getAssignments(userId);
      this._checkAccessAssignments[userId] = assignments;
    }

    if ((!assignments || _.keys(assignments).length === 0) && this._defaultRoles.length === 0) {
      return false;
    }

    await this.loadFromCache();

    if (this.items && _.keys(this.items).length > 0) {
      return this._checkAccessFromCache(userId, authId, params, assignments);
    }

    return this._checkAccessRecursive(userId, authId, params, assignments);
  }

  async _checkAccessFromCache(userId, authId, params, assignments) {
    if (!this.items.hasOwnProperty(authId)) {
      return false;
    }

    const item = this.items[authId];

    if (!await this._executeRule(userId, item, params)) {
      return false;
    }

    if (assignments.hasOwnProperty(authId) || _.includes(this.defaultRoles, authId)) {
      return true;
    }

    if (this.parents.hasOwnProperty(authId)) {
      for (let i = 0; i < this.parents[authId].length; ++i) {
        if (await this._checkAccessFromCache(userId, this.parents[authId][i], params, assignments)) {
          return true;
        }
      }
    }

    return false;
  }

  async _checkAccessRecursive(userId, authId, params, assignments) {
    const item = await this._getItem(authId);
    if (!item) {
      return false;
    }

    if (!await this._executeRule(userId, item, params)) {
      return false;
    }

    if (assignments.hasOwnProperty(authId) || _.includes(this.defaultRoles, authId)) {
      return true;
    }

    const parents = await AuthChild.query().where({ child_id: authId }).column('parent_id').pluck('parent_id');
    for (let i = 0; i < parents.length; ++i) {
      if (await this._checkAccessRecursive(userId, parents[i], params, assignments)) {
        return true;
      }
    }
  }

  async _executeRule(userId, item, params) {
    // TODO implement rule check
    return true;
  }

  async loadFromCache() {
    if ((this.items && _.keys(this.items).length > 0) || !this.cache) {
      return;
    }

    const data = this.cache.get(this.cacheKey);
    
    if (_.isArray(data) && data.length === 2 && data[0] && data[1]) {
      this.items = data[0];
      this.parents = data[1];
      return;
    }

    const items = await Auth.query();
    this.items = _.keyBy(items, 'id');

    const authchilds = await AuthChild.query();
    this.parents = {};
    for(let i = 0; i < authchilds.length; ++i) {
      if (!this.parents.hasOwnProperty(authchilds[i].childId)) {
        this.parents[authchilds[i].childId] = [];
      }
      this.parents[authchilds[i].childId].push(authchilds[i].parentId);
    }

    this.cache.set(this.cacheKey, [this.items, this.parents]);
  }

  async add(auth) {
    return Auth.query().insert({...auth}).returning('*');
  }

  async update(id, auth) {
    return Auth.query().patch({...auth}).where({ id }).returning('*').first();
  }

  async delete(auth) {
    const id = typeof(auth) === 'Auth' ? auth.id : auth;
    return Auth.query().deleteById(id);
  }

  async getRoles() {
    return this._getItems(Auth.TYPE_ROLE);
  }

  async getRole(roleId) {
    return Auth.query().findOne({ id: roleId, type: Auth.TYPE_ROLE });
  }

  async getRolesByUser(userId) {
    return Auth.query()
      .join('user_auth', function() {
        this
          .on('auth.id', '=', 'user_auth.auth_id')
          .andOn('user_auth.user_id', '=', userId);
      });
  }

  async getChildRoles(roleId) {
    const role = await this.getRole(roleId);
    if (!role) {
      throw new Error(`Role "${roleId}" not found`);
    }

    let result = [];
    this._getChildrenRecursive(roleId, await this._getChildrenList(), result);

    let roles = { [roleId]: role };
    _.each(await this.getRoles(), function(value) {
      if (_.includes(result, value.id)) {
        roles[value.id] = value;
      }
    })

    return roles;
  }

  async getPermissions() {
    return this._getItems(Auth.TYPE_PERMISSION);
  }

  async getPermission(permissionId) {
    return Auth.query().findOne({ id: permissionId, type: Auth.TYPE_PERMISSION });
  }

  async getPermissionsByRole(roleId) {
    let result = [];
    this._getChildrenRecursive(roleId, await this._getChildrenList(), result);
    if (result.length === 0) {
      return {};
    }

    let permissions = await Auth.query()
      .whereIn('id', result)
      .andWhere({ type: Auth.TYPE_PERMISSION });

    return _.keyBy(permissions, 'id');
  }

  async getPermissionsByUser(userId) {
    if (!userId) {
      return {};
    }

    return _.merge(
      await this.getDirectPermissionsByUser(userId), 
      await this.getInheritedPermissionsByUser(userId)
    );
  }

  async getRules() {
    return this._getItems(Auth.TYPE_RULE);
  }

  async getRule(ruleId) {
    return Auth.query().findOne({ id: ruleId, type: Auth.TYPE_RULE });
  }

  async canAddChild(parent, child) {
    return !await this.detectLoop(parent, child);
  }

  async addChild(parent, child) {
    if (parent.id === child.id) {
      throw new Error(`Cannot add '${parent.id}' as a child of itself.`);
    }

    if (parent.type === Auth.TYPE_PERMISSION && child.type === Auth.TYPE_ROLE) {
      throw new Error('Cannot add a role as a child of a permission.');
    }

    if (this.detectLoop(parent, child)) {
      throw new Error(`Cannot add '${child.id}' as a child of '${parent.id}'. A loop has been detected.`);
    }

    return AuthChild.query().insert({ parent_id: parent.id, child_id: child.id }).returning('*');
    
  }

  async removeChild(parent, child) {
    return AuthChild.query().where({ parent_id: parent.id, child_id: child.id }).delete();
  }

  async removeChildren(parent) {
    return AuthChild.query().where('parent_id', parent.id).delete();
  }

  async hasChild(parent, child) {
    return (await AuthChild.query().where({ parent_id: parent.id, child_id: child.id }).count(AuthChild.raw(1)).first()).count > 0;
  }

  async getChildren(parentId) {
    const parent = await Auth.query().eager('children').findOne('id', parentId);
    return parent.children;
  }

  async assign(authId, userId) {
    delete this._checkAccessAssignments[userId];
    const exists = (await UserAuth.query().where({ user_id: userId, auth_id: authId }).count(UserAuth.raw(1)).first()).count > 0;
    if (exists) {
      return false;
    }
    return UserAuth.query().insert({ user_id: userId, auth_id: authId }).returning('*');
  }

  async revoke(authId, userId) {
    if (!userId) {
      return false;
    }
    delete this._checkAccessAssignments[userId];
    return UserAuth.query().delete({ user_id: userId, auth_id: authId });
  }

  async revokeAll(userId) {
    if (!userId) {
      return false;
    }
    delete this._checkAccessAssignments[userId];
    return UserAuth.query().delete({ user_id: userId });
  }

  async getAssignment(authId, userId) {
    if (!userId) {
      return null;
    }
    return UserAuth.query().findOne({ user_id: userId, auth_id: authId });
  }

  async getAssignments(userId) {
    if (!userId) {
      return {};
    }
    const results = await UserAuth.query().where({ user_id: userId });
    return _.keyBy(results, 'authId');
  }

  async getUserIdsByRole(roleId) {
    if (!roleId) {
      return [];
    }
    return UserAuth.query()
      .join('auth', function() {
        this
          .on('user_auth.auth_id', '=', 'auth.id')
          .andOn('auth.type', '=', Auth.TYPE_ROLE)
      })
      .column('user_id').pluck('user_id');
  }

  async removeAll() {
    this.removeAllAssignments();
    await Auth.query().delete();
    await AuthChild.query().delete();
    await UserAuth.query().delete();
    this.invalidateCache()
  }

  async removeAllPermissions() {
    await this._removeAllItems(Auth.TYPE_PERMISSION);
  }

  async removeAllRoles() {
    await this._removeAllItems(Auth.TYPE_ROLE);
  }

  async removeAllRules() {
    await this._removeAllItems(Auth.TYPE_RULE);
  }

  async removeAllAssignments() {
    this._checkAccessAssignments = {};
    await UserAuth.query().delete();
  }

  async _removeAllItems(type) {
    await Auth.query().where({ type }).delete();
  }

  async detectLoop(parent, child) {
    if (child.id === parent.id) {
      return true;
    }

    const children = await this.getChildren(child.id);
    const keys = _.keys(children);

    for(let i = 0; i < keys.length; i++) {
      if (await this.detectLoop(parent, children[keys[i]])) {
        return true;
      }
    }

    return false;
  }

  async _getItems(type) {
    let results = await Auth.query().where('type', type);
    return _.keyBy(results, 'id');
  }

  async _getItem(id) {
    return Auth.query().findOne({ id });
  }

  async _getChildrenList() {
    let parents = {};
    let result = await AuthChild.query();
    _.forEach(result, function(value) {
      if (!parents.hasOwnProperty(value.parentId)) {
        parents[value.parentId] = [];
      }
      parents[value.parentId].push(value.childId);
    });
    return parents;
  }

  _getChildrenRecursive(name, childrenList, result) {
    if (childrenList.hasOwnProperty(name)) {
      _.each(childrenList[name], (value) => {
        result.push(value);
        this._getChildrenRecursive(value, childrenList, result);
      })
    }
  }

  async _getDirectPermissionsByUser(userId) {
    const permissions = await Auth.query()
      .join('user_auth', function() {
        this
          .on('auth.id', '=', 'user_auth.auth_id')
          .andOn('user_auth.user_id', '=', userId)
          .andOn('auth.type', '=', Auth.TYPE_PERMISSION);
      });
    return _.keyBy(permissions, 'id');
  }

  async _getInheritedPermissionsByUser(userId) {
    const roles = await Auth.query()
      .join('user_auth', function() {
        this
          .on('auth.id', '=', 'user_auth.auth_id')
          .andOn('user_auth.user_id', '=', userId)
          .andOn('auth.type', '=', Auth.TYPE_ROLE);
      });
    const childrenList = await this._getChildrenList();
    let result = [];
    _.each(roles, (role) => {
      this._getChildrenRecursive(role.id, childrenList, result);
    });

    if (result.length === 0) {
      return {};
    }

    let permissions = await Auth.query()
        .whereIn('id', result)
        .andWhere({ type: Auth.TYPE_PERMISSION });

    return _.keyBy(permissions, 'id');
  }

  invalidateCache() {
    if (this.cache) {
      this.cache.delete(this.cacheKey);
      this.items   = {};
      this.parents = {};
    }
    this._checkAccessAssignments = {};
  }

};