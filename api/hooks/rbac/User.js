'use strict';

const _ = require('lodash');

module.exports = class User {
  
  constructor(token, identity) {
    this.cache          = sails.config.rbac.cache || sails.cache;
    this.cacheKeyPrefix = sails.config.rbac.userCacheKeyPrefix || 'userAccess';
    this.loginUrl       = sails.config.rbac.loginUrl || '/auth/login';
    this.allowActions   = sails.config.rbac.allowActions || [];
    this._identity      = identity;
    this._token         = token;
  }

  get identity() {
    return this._identity;
  }

  get token() {
    return this._token;
  }

  get isGuest() {
    return this.identity === null;
  }

  get id() {
    return this.identity !== null ? this.identity.id : null;
  }

  get cacheKey() {
    return [this.cacheKeyPrefix, this.id].join('');
  }

  async can(authId, params = {}, allowCaching = true) {

    if (_.includes(this.allowActions, authId)) {
      return true;
    }

    let access = false;
    if (allowCaching && _.isEmpty(params) && await this.cache.exists(this.cacheKey)) {
      let cache = await this.cache.get(this.cacheKey);
      if (cache && cache.hasOwnProperty(authId)) {
        return cache[authId];
      }
    }

    access = await sails.rbac.manager.checkAccess(this.id, authId, params);

    if (allowCaching && _.isEmpty(params)) {
      let cache = await this.cache.exists(this.cacheKey) ? await this.cache.get(this.cacheKey, {}) : {};
      cache[authId] = access;
      await this.cache.set(this.cacheKey, cache); 
    }

    return access;
  }

}