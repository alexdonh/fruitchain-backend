'use strict';

/**
 * kue hook
 *
 * @description :: A hook definition.  Extends Sails by adding shadow routes, implicit actions, and/or initialization logic.
 * @docs        :: https://sailsjs.com/docs/concepts/extending-sails/hooks
 */

const kue = require('kue');

module.exports = function (sails) {

  let queue;

  return {

    defaults: {
      __configKey__: {
        active: true,
        //default key prefix for kue in
        //redis server
        prefix: 'q',

        //default redis configuration
        redis: {
            //default redis server port
            port: 6379,
            //default redis server host
            host: '127.0.0.1'
        },
        //number of milliseconds
        //to wait 
        //before shutdown publisher
        shutdownDelay: 5000
      }
    },

    /**
     * Runs when this Sails app loads/lifts.
     */
    initialize: async function(done) {
      const config = sails.config[this.configKey];

      if (!config.active) {
        return done();
      }
      
      this.instance = queue = kue.createQueue(config);
      sails.on('lower', () => {
        queue
          .shutdown(config.shutdownDelay, function(error) {
            sails.emit('kue:shutdown', error || '');
          });
      });
      sails.on('lifted', () => {
        sails.log.verbose('Kue loaded successfully');
      });
      sails.emit('hook:kue:loaded');
      return done();
    },

    // create: function(key, data) {
    //   return this.instance.create(key, data);
    // },

    dispatch: function (key, data, { priority = 'normal', attempts = 1, remove = true, jobFn = () => {} } = {}) {
      if (typeof key !== 'string') {
        throw new Error(`Expected job key to be of type string but got <${typeof key}>.`);
      }

      const job = this.instance
        .create(key, data)
        .priority(priority)
        .attempts(attempts)
        .removeOnComplete(remove);

      jobFn(job);

      job.save(err => {
        if (err) {
          sails.log.error('An error has occurred while creating a Kue job.')
          throw err
        }
      });

      job.result = new Promise((resolve, reject) => {
        job.on('complete', result => {
          resolve(result)
        })
      });
  
      return job;
    }

  };

};
