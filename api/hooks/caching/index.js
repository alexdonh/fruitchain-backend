'use strict';

const Cache = require('./Cache');
const DummyCache = require('./engine/Dummy');
const FileCache = require('./engine/File');

/**
 * cache hook
 *
 * @description :: A hook definition.  Extends Sails by adding shadow routes, implicit actions, and/or initialization logic.
 * @docs        :: https://sailsjs.com/docs/concepts/extending-sails/hooks
 */

module.exports = function (sails) {

  return {

    defaults: {
      __configKey__: {
        active: true,
        engine: 'file',
        keyPrefix: null,
        defaultDuration: 0
      }
    },

    configure: function() {
      const config = sails.config[this.configKey];

      if (!_.includes(Cache.ENGINES, config.engine)) {
        config.engine = 'dummy';
      }
    },

    /**
     * Runs when this Sails app loads/lifts.
     */
    initialize: async function(done) {
      const config = sails.config[this.configKey];
      
      switch(config.engine) {
        default:
        case 'dummy':
          this.engine = new DummyCache(config);
        case 'file':
          this.engine = new FileCache(config);
      }

      sails.cache = this.engine;

      sails.emit('hook:cache:loaded');
      return done();
    }

  };

};
