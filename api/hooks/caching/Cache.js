'use strict';

const _ = require('lodash');
const crypto = require('crypto');

class Cache {
  constructor(config) {
    this.keyPrefix = config.keyPrefix || '';
    this.defaultDuration = config.defaultDuration || 0;
  }

  buildKey(key) {
    key = crypto.createHash('md5').update(_.isString(key) ? key : JSON.stringify(key)).digest('hex');
    return this.keyPrefix + key;
  }

  async get(key) {
    key = this.buildKey(key);
    let value = await this.getValue(key);
    return value === false ? value : JSON.parse(value);
  }

  async exists(key) {
    value = await this.get(key);
    return value !== false;
  }

  async multiGet(keys) {
    keyMap = keys.reduce((prev, curr) => {
      prev[curr] = this.buildKey(curr);
      return prev;
    }, {});

    values = await this.getValues(_.keys(keyMap));
    return _.mapValues(values, (value) => value === false ? value : JSON.parse(value));
  }

  async set(key, value, duration = null) {
    return this.setValue(this.buildKey(key), JSON.stringify(value), duration || this.defaultDuration);
  }

  async multiSet(items, duration = null) {
    let data = {};
    _.each(items, (value, key) => {
      data[this.buildKey(key)] = JSON.stringify(value);
    })
    return this.setValues(data, duration = duration || this.defaultDuration);
  }

  async add(key, value, duration = null) {
    return this.addValue(this.buildKey(key), JSON.stringify(value), duration || this.defaultDuration);
  }

  async multiAdd(items, duration = null) {
    let data = {};
    _.each(items, (value, key) => {
      data[this.buildKey(key)] = JSON.stringify(value);
    })
    return this.addValues(data, duration || this.defaultDuration);
  }

  async delete(key) {
    return this.deleteValue(this.buildKey(key));
  }

  async flush() {
    return this.flushValues();
  }

  async getValue(key) {
    return false;
  }

  async setValue(key, value, duration) {
    return true;
  }

  async addValue(key, value, duration) {
    return true;
  }

  async deleteValue(key) {
    return true;
  }

  async flushValues() {
    return true;
  }

  async getValues(keys) {
    let results = {};
    _.each(keys, async (key) => {
      results[key] = await this.getValue(key);
    })

    return results;
  }

  async setValues(data, duration) {
    let failedKeys = [];
    _.each(data, async (value, key) => {
      if (await this.setValue(key, value, duration) === false) {
        failedKeys.push(key);
      }
    })

    return failedKeys;
  }

  async addValues() {
    let failedKeys = [];
    _.each(data, async (value, key) => {
      if (await this.addValue(key, value, duration) === false) {
        failedKeys.push(key);
      }
    })

    return failedKeys;
  }

  async getOrSet(key, cb, duration = null) {
    let value = await this.get(key);
    if (value !== false) {
      return value;
    }

    value = cb.call(this);

    if (!await this.set(key, value, duration)) {
      sails.log.warn(`Failed to set cache value for key "${JSON.stringify(key)}"`);
    }

    return value;
  }
}

Cache.ENGINES = ['file'];

module.exports = Cache;