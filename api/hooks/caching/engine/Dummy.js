'use strict';

const Cache = require('../Cache');

module.exports = class Dummy extends Cache {
  constructor(config) {
    super(config);
  }

  async getValue(key) {
    return false;
  }

  async setValue(key, value, duration) {
    return true;
  }

  async addValue(key, value, duration) {
    return true;
  }

  async deleteValue(key) {
    return true;
  }

  async flushValues() {
    return true;
  }
}