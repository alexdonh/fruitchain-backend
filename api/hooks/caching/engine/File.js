'use strict';

const path  = require('path');
const fs    = require('fs');
const Cache = require('../Cache');

module.exports = class File extends Cache {
  constructor(config) {
    super(config);
    this.cachePath      = config.cachePath || path.join(sails.config.appPath, 'runtime', 'cache');
    this.directoryLevel = config.directoryLevel || 1;
    this.gcProbability  = config.gcProbability || .05;
    this.fileMode       = config.fileMode || 0o644;
    this.dirMode        = config.dirMode || 0o755;

    if (!fs.existsSync(this.cachePath)) {
      fs.mkdirSync(this.cachePath, { recursive: true, mode: this.dirMode });
    }
  }

  _getCacheFile(key) {
    if (this.directoryLevel > 0) {
      let base = this.cachePath;
      for(let i = 0; i < this.directoryLevel; ++i) {
        let prefix = key.substr(i + i, 2);
        if (prefix) {
          base = path.join(base, prefix);
        }
      }
      return path.join(base, key)
    }

    return path.join(this.cachePath, key);
  }

  async _gc(force = false, expiredOnly = true) {
    if (force || Math.random() < this.gcProbability) {
      await this._gcRecursive(this.cachePath, expiredOnly);
    }
  }

  async _gcRecursive(filePath, expiredOnly) {
    try {
      const files = await fs.promises.readdir(filePath, { encoding: 'utf8' });
      for(let i = 0; i < files.length; ++i) {
        try {
          let subPath = path.join(filePath, files[i]);
          let stats = await fs.promises.stat(subPath);
          if (stats.isDirectory()) {
            await this._gcRecursive(subPath, expiredOnly);
            if (!expiredOnly) {
              await fs.promises.rmdir(subPath).catch((err) => { sails.log.warn(err) });
            }
          } else if (!expiredOnly || expiredOnly && new Date(stats.mdate) < new Date()) {
            await fs.promises.unlink(subPath).catch((err) => { sails.log.warn(err) });
          }
        } catch(err) {
          continue;
        }
      }
    } catch(err) {
      sails.log.warn(err);
    }
  }

  async exists(key) {
    key = this.buildKey(key);
    const cacheFile = this._getCacheFile(key);
    try {
      const stats = await fs.promises.stat(cacheFile);
      return new Date(stats.mtime) > new Date();
    } catch(err) {
      sails.log.warn(err);
    }
    return false;
  }

  async getValue(key) {
    const cacheFile = this._getCacheFile(key);
    try {
      const stats = await fs.promises.stat(cacheFile);
      if (new Date(stats.mtime) > new Date()) {
        return fs.promises.readFile(cacheFile, 'utf8') || false;
      }
    } catch(err) {
      sails.log.warn(err);
    }
    return false;
  }

  async setValue(key, value, duration) {
    await this._gc(this.cachePath, this.probability);
    const cacheFile = this._getCacheFile(key);
    if (this.directoryLevel > 0) {
      await fs.promises.mkdir(path.dirname(cacheFile), { recursive: true, mode: this.dirMode });
    }
    try {
      if (fs.existsSync(cacheFile)) {
        const stats = await fs.promises.stat(cacheFile);
        if (stats && stats.isFile() && process.geteuid && process.geteuid() !== stats.uid) {
          await fs.promises.unlink(cacheFile);
        }
      }
      if (duration <= 0) {
        duration = 31536000; // 1 year
      }
      await fs.promises.writeFile(cacheFile, value, { encoding: 'utf8', mode: this.fileMode });
      let mtime = new Date();
      mtime.setSeconds(duration);
      await fs.promises.utimes(cacheFile, new Date(), mtime);
      return true;
    }
    catch(err) {
      sails.log.warn(err);
      return false
    }
  }

  async addValue(key, value, duration) {
    const cacheFile = this._getCacheFile(key);
    const stats = await fs.promises.stat(cacheFile);
    if (new Date(stats.mtime) > new Date()) {
      return false;
    }
    return this.setValue(key, value, duration);
  }

  async deleteValue(key) {
    const cacheFile = this._getCacheFile(key);
    return fs.promises.unlink(cacheFile).catch((err) => { sails.log.warn(err) });
  }

  async flushValues() {
    await this._gc(true, false);
  }
}