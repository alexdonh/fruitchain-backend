'use strict';

// Ensure we're in the project directory, so cwd-relative paths work as expected
// no matter where we actually lift from.
// > Note: This is not required in order to lift, but it is a convenient default.
process.chdir(__dirname);

let sails;

try {
  sails = require('sails');
} catch (err) {
  console.error('Encountered an error when attempting to require(\'sails\'):');
  console.error(err.stack);
  console.error('--');
  console.error('To run an app using `node app.js`, you need to have Sails installed');
  console.error('locally (`./node_modules/sails`).  To do that, just make sure you\'re');
  console.error('in the same directory as your app and run `npm install`.');
  return;
}//-•

sails.load({
	hooks: {
		blueprints  : false,
		controllers : false,
		cors        : false,
		csrf        : false,
		grunt       : false,
		http        : false,
		i18n        : false,
		kue         : false,
		policies    : false,
		pubsub      : false,
		request     : false,
		responses   : false,
		session     : false,
		sockets     : false,
    views       : false,
    watch       : false
  },
  models: {
    migrate: 'safe'
  }
}, function (err) {

	if (err) {
    sails.log.error('This script relies on access to Sails, but when attempting to load this Sails app automatically, an error occurred. Details:');
    throw err;
  }

  const config = sails.config.kue;

  if (!config) {
    throw new Error('Configuration for Kue must be specified in config/kue.js.');
  }

  const _ = require('@sailshq/lodash');
      
  let events = [];

  if (sails.hooks.objection) {
    events.push('hook:objection:loaded');
  }
  
  sails.after(events, () => {
    sails.log.info('Kue started.');

    const queue = require('kue').createQueue(config);

    var jobs = require('include-all')({
      dirname     :  sails.config.appPath + '/api/jobs',
      filter      :  /(.+)\.js$/,
      excludeDirs :  /^\.(git|svn)$/,
      optional    :  true
    });

    _.forEach(jobs, function (job, name) {
      queue.process(name, job.concurrency || 1, job.fn);
    });

    function shutdown(sig) {
      queue.shutdown(config.shutdownDelay, function (err) {
        sails.emit('kue:shutdown', err || '');
        sails.log.verbose('Kue shutdown successfully.', err || '');
        process.exit(0);
      });
    }

    process.once('SIGTERM', shutdown);
    process.once('SIGINT', shutdown);
  });

});